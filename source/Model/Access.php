<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Model;

use HomeHealth\Zika\Model;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.0.0
 */
final class Access extends Model\Base\Access
{
    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function getIPAddress(): ?string
    {
        return parent::getIPAddress() !== null ? inet_ntop(parent::getIPAddress()) : parent::getIPAddress();
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function setIPAddress($address): self
    {
        return parent::setIPAddress(inet_pton($address));
    }
}
