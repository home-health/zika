<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Model;

use HomeHealth\Zika\Model;
use Propel\Runtime\Connection\ConnectionInterface;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.0.0
 */
final class Account extends Model\Base\Account
{
    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function preInsert(ConnectionInterface $connection = null): bool
    {
        $this->addDisplay(new Model\Display());
        return true;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function preSave(ConnectionInterface $connection = null): bool
    {
        if (password_needs_rehash($this->getPassword(), PASSWORD_BCRYPT)) {
            $this->setPassword(password_hash($this->getPassword(), PASSWORD_BCRYPT));
        }
        return true;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function preUpdate(ConnectionInterface $connection = null): bool
    {
        if ($this->isColumnModified(Model\Map\AccountTableMap::COL_EMAIL_ADDRESS)) {
            $this->setEmailVerified(false);
        }
        return true;
    }
}
