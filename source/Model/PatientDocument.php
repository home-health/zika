<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Model;

use HomeHealth\Zika\Model;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.5.0
 */
final class PatientDocument extends Model\Base\PatientDocument
{
    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.5.0
     */
    public function getData(): ?string
    {
        return parent::getData() !== null ? stream_get_contents(parent::getData()) : parent::getData();
    }
}
