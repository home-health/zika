<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Model;

use DateTime;
use HomeHealth\Zika\Model;
use HomeHealth\Zika\Utility\Model\Encoder;
use Propel\Runtime\Map\TableMap;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.0.0
 */
final class Validation extends Model\Base\Validation
{
    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function getCredential(): string
    {
        return (new Encoder\JWT(getenv('JWT_SECRET_KEY_VALIDATION')))->encode([
            'account_identifier' => $this->getAccountIdentifier(),
            'iat'                => $this->getCreatedAt() ? $this->getCreatedAt()->getTimestamp() : null,
            'jti'                => $this->getIdentifier(),
            'exp'                => $this->getExpiredAt() ? $this->getExpiredAt()->getTimestamp() : null
        ]);
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.3.0
     */
    public function getExpiredAt(): ?DateTime
    {
        if ($this->getUpdatedAt() !== null) {
            return $this->getUpdatedAt()->modify('+ 5 minutes');
        }
        return null;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function toArray($type = TableMap::TYPE_FIELDNAME, $lazy = true, $dumped = [], $foreign = false): array
    {
        return array_merge(parent::toArray($type, $lazy, $dumped, $foreign), ['credential' => $this->getCredential()]);
    }
}
