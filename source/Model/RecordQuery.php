<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Model;

use HomeHealth\Zika\Model;
use HomeHealth\Zika\Utility\Model\Formatter;
use Propel\Runtime\Formatter\SimpleArrayFormatter;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.3.0
 */
abstract class RecordQuery extends Model\Base\RecordQuery
{
    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.6.0
     */
    abstract public function filterBySession($session, $comparison = null);

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.6.0
     */
    abstract public function filterByValidation($validation, $comparison = null);

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.6.0
     */
    public function configureSelectColumns(): void
    {
        parent::configureSelectColumns();
        if ($this->getFormatter() instanceof SimpleArrayFormatter) {
            $this->setFormatter(Formatter\Vector::class);
        }
    }
}
