<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Model;

use HomeHealth\Zika\Model;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.4.0
 */
abstract class GeolocationQuery extends Model\Base\GeolocationQuery
{
}
