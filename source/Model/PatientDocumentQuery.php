<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Model;

use HomeHealth\Zika\Model;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.5.0
 */
final class PatientDocumentQuery extends Model\Base\PatientDocumentQuery
{
    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.6.0
     */
    public function filterBySession($session, $comparison = null)
    {
        return $this->usePatientQuery()->filterByAccountIdentifier(
            $session->getAccountIdentifier(),
            $comparison
        )->endUse();
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.6.0
     */
    public function filterByValidation($validation, $comparison = null)
    {
        return $this->usePatientQuery()->filterByAccountIdentifier(
            $validation->getAccountIdentifier(),
            $comparison
        )->endUse();
    }
}
