<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Model;

use HomeHealth\Zika\Model;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.0.0
 */
final class AccessQuery extends Model\Base\AccessQuery
{
    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.6.0
     */
    public function filterBySession($session, $comparison = null)
    {
        return $this->filterByAccountIdentifier(
            $session->getAccountIdentifier(),
            $comparison
        );
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.6.0
     */
    public function filterByValidation($validation, $comparison = null)
    {
        return $this->filterByAccountIdentifier(
            $validation->getAccountIdentifier(),
            $comparison
        );
    }
}
