<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Utility\Email\Transport;

use HomeHealth\Zika\Utility\Email\Transport;
use Symfony\Component\Mailer\Transport\Smtp\EsmtpTransport;
use Symfony\Component\Mailer\Transport\TransportInterface;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.0.0
 */
final class Factory
{
    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function create(): TransportInterface
    {
        return (
            new Transport\Configuration(
                new EsmtpTransport()
            )
        )->perform()->getTransport();
    }
}
