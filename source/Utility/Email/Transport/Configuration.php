<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Utility\Email\Transport;

use Symfony\Component\Mailer\Transport\TransportInterface;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.0.0
 */
final class Configuration
{
    private $transport;

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function __construct(TransportInterface $transport)
    {
        $this->setTransport($transport);
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function getTransport(): TransportInterface
    {
        return $this->transport;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function perform(): self
    {
        $this->getTransport()->setUsername(
            getenv('SMTP_ACCOUNT_EMAIL_ADDRESS')
        )->setPassword(
            getenv('SMTP_ACCOUNT_PASSWORD')
        )->getStream()->setHost(
            getenv('SMTP_HOST_ADDRESS')
        )->setPort(intval(
            getenv('SMTP_HOST_PORT')
        ));
        return $this;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function setTransport(TransportInterface $transport): self
    {
        $this->transport = $transport;
        return $this;
    }
}
