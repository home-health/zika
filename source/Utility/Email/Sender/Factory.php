<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Utility\Email\Sender;

use HomeHealth\Zika\Utility\Email\Transport;
use Symfony\Component\Mailer\Mailer;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.0.0
 */
final class Factory
{
    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function create(): Mailer
    {
        return new Mailer(
            (new Transport\Factory())->create()
        );
    }
}
