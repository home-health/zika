<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Utility\Email\Message;

use Symfony\Component\Mime;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.0.0
 */
final class Configuration
{
    private $message;

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function __construct(Mime\Email $message)
    {
        $this->setMessage($message);
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function getMessage(): Mime\Email
    {
        return $this->message;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function perform(): self
    {
        $this->getMessage()->from(new Mime\Address(getenv('SMTP_ACCOUNT_EMAIL_ADDRESS'), getenv('SMTP_ACCOUNT_NAME')));
        return $this;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function setMessage(Mime\Email $message): self
    {
        $this->message = $message;
        return $this;
    }
}
