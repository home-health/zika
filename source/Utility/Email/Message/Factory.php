<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Utility\Email\Message;

use HomeHealth\Zika\Utility\Email\Message;
use Symfony\Component\Mime;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.0.0
 */
final class Factory
{
    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function create(): Mime\Email
    {
        return (
            new Message\Configuration(
                new Mime\Email()
            )
        )->perform()->getMessage();
    }
}
