<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Utility\Model\Formatter;

use Propel\Runtime\Formatter\SimpleArrayFormatter;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.6.0
 */
final class Vector extends SimpleArrayFormatter
{
    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.6.0
     */
    public function getAsColumns(): array
    {
        return array_combine(
            array_map(
                function ($column) {
                    return str_replace('"', '', $column);
                },
                array_keys(parent::getAsColumns())
            ),
            array_values(parent::getAsColumns())
        );
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.6.0
     */
    public function getContainer(): array
    {
        return array_fill(0, $this->getTableMap()::NUM_COLUMNS, null);
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.6.0
     */
    public function getIntersected(array $row): array
    {
        return array_intersect_key($row, $this->getAsColumns());
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.6.0
     */
    public function getPopulated(array $row): array
    {
        return $this->getTableMap()->populateobject($row)[0]->toArray();
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.6.0
     */
    public function getSanitized(array $row): array
    {
        return array_replace($this->getContainer(), array_combine($this->getSelected(), array_values($row)));
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.6.0
     */
    public function getSelected(): array
    {
        return array_map(
            function ($column) {
                return $this->getTableMap()->translateFieldName(
                    $column,
                    $this->getTableMap()::TYPE_FIELDNAME,
                    $this->getTableMap()::TYPE_NUM
                );
            },
            array_keys($this->getAsColumns())
        );
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.6.0
     */
    public function getStructuredArrayFromRow($row): array
    {
        return $this->getIntersected($this->getPopulated($this->getSanitized($row)));
    }
}
