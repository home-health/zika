<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Utility\Model\Parser;

use HomeHealth\Zika\Utility\Model\Covenant;
use InvalidArgumentException;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Map\TableMap;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.0.0
 */
final class Record implements Covenant\Parser
{
    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function assemble(array $array, string $class): object
    {
        if (is_a($class, ActiveRecordInterface::class, true)) {
            ($object = new $class())->fromArray($array, TableMap::TYPE_FIELDNAME);
            return $object;
        }
        throw new InvalidArgumentException();
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function dismount(object $object): array
    {
        if ($object instanceof ActiveRecordInterface) {
            return $this->process($object->toArray(TableMap::TYPE_FIELDNAME, true, []));
        }
        throw new InvalidArgumentException();
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    private function filter(array $array): array
    {
        return array_filter(
            $array,
            function ($value, $index) {
                return $value !== '*RECURSION*' && strpos(strval($index), '_identifier') === false;
            },
            ARRAY_FILTER_USE_BOTH
        );
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    private function process(array $array): array
    {
        $array = $this->sort($this->filter($array));
        foreach ($array as &$value) {
            if (is_array($value)) {
                $value = $this->process($value);
            }
        }
        return $array;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    private function sort(array $array): array
    {
        ksort($array);
        return $array;
    }
}
