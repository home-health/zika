<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Utility\Model\Encoder;

use Firebase\JWT\JWT as Encoder;
use HomeHealth\Zika\Utility\Model\Covenant;
use Throwable;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.0.0
 */
final class JWT implements Covenant\Encoder
{
    private $secret;

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function __construct(string $secret)
    {
        $this->setSecret($secret);
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function decode(string $string): array
    {
        try {
            return json_decode(json_encode(Encoder::decode($string, $this->getSecret(), ['HS512'])), true);
        } catch (Throwable $throwable) {
            throw new JWT\Exception($throwable->getMessage());
        }
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function encode(array $array): string
    {
        try {
            return Encoder::encode($array, $this->getSecret(), 'HS512');
        } catch (Throwable $throwable) {
            throw new JWT\Exception($throwable->getMessage());
        }
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function getSecret(): string
    {
        return $this->secret;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function setSecret(string $secret): self
    {
        $this->secret = $secret;
        return $this;
    }
}
