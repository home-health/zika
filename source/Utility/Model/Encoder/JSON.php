<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Utility\Model\Encoder;

use HomeHealth\Zika\Utility\Model\Covenant;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.0.0
 */
final class JSON implements Covenant\Encoder
{
    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function decode(string $string): array
    {
        return json_decode($string, true);
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function encode(array $array): string
    {
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }
}
