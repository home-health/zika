<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Utility\Model\Encoder\JWT;

use UnexpectedValueException;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.0.0
 */
final class Exception extends UnexpectedValueException
{
}
