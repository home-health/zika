<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Utility\Translation\Resource\Brazil;

use HomeHealth\Zika\Utility\Translation\Covenant;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.0.0
 */
final class Portuguese implements Covenant\Resource
{
    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function getDefinition(): array
    {
        return [
            'validation' => [
                'credential' => [
                    'generated' => 'Uma credencial de validação foi gerada'
                ]
            ]
        ];
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function getLanguage(): string
    {
        return 'pt_BR';
    }
}
