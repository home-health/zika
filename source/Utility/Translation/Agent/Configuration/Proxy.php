<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Utility\Translation\Agent\Configuration;

use Symfony\Component\Translation\Loader;
use Symfony\Component\Translation\Translator;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.0.0
 */
final class Proxy
{
    private $agent;

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function __construct(Translator $agent)
    {
        $this->setAgent($agent);
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function getAgent(): Translator
    {
        return $this->agent;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function setLoader(Loader\ArrayLoader $loader): self
    {
        $this->getAgent()->addLoader('array', $loader);
        return $this;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function setResources(array $resources): self
    {
        foreach ($resources as $resource) {
            $this->getAgent()->addResource('array', $resource->getDefinition(), $resource->getLanguage());
        }
        $this->getAgent()->setFallbackLocales(
            array_map(
                function ($resource) {
                    return $resource->getLanguage();
                },
                $resources
            )
        );
        return $this;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function setAgent(Translator $agent): self
    {
        $this->agent = $agent;
        return $this;
    }
}
