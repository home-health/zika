<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Utility\Translation\Agent;

use HomeHealth\Zika\Utility\Translation\Agent\Configuration;
use Symfony\Component\Translation\Translator;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.0.0
 */
final class Factory
{
    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function create(string $language): Translator
    {
        return (
            new Configuration(
                new Configuration\Proxy(
                    new Translator($language)
                )
            )
        )->perform()->getProxy()->getAgent();
    }
}
