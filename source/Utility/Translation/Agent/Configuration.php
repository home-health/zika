<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Utility\Translation\Agent;

use HomeHealth\Zika\Utility\Translation\Resource;
use Symfony\Component\Translation\Loader;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.0.0
 */
final class Configuration
{
    private $proxy;

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function __construct(Configuration\Proxy $proxy)
    {
        $this->setProxy($proxy);
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function getProxy(): Configuration\Proxy
    {
        return $this->proxy;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function perform(): self
    {
        $this->getProxy()->setLoader(
            new Loader\ArrayLoader()
        )->setResources([
            new Resource\Brazil\Portuguese(),
            new Resource\UnitedStates\English()
        ]);
        return $this;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function setProxy(Configuration\Proxy $proxy): self
    {
        $this->proxy = $proxy;
        return $this;
    }
}
