<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Utility\App;

use HomeHealth\Zika\Handler\Resolver;
use HomeHealth\Zika\Middleware;
use HomeHealth\Zika\Route;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.0.0
 */
final class Configuration
{
    private $proxy;

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function __construct(Configuration\Proxy $proxy)
    {
        $this->setProxy($proxy);
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function getProxy(): Configuration\Proxy
    {
        return $this->proxy;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function perform(): self
    {
        $this->getProxy()->setMiddlewares([
            new Middleware\Exception([
                new Resolver\Exception\HTTP(),
                new Resolver\Exception\JWT(),
                new Resolver\Exception\PDO()
            ]),
            new Middleware\Content\Length(),
            new Middleware\Content\Type(),
            new Middleware\CORS(
                $this->getProxy()->getApp()->getRouteResolver()
            )
        ])->setRoutes([
            new Route\Configuration\Access(),
            new Route\Configuration\Account(),
            new Route\Configuration\Display(),
            new Route\Configuration\Patient(),
            new Route\Configuration\PatientAddress(),
            new Route\Configuration\PatientDocument(),
            new Route\Configuration\Session(),
            new Route\Configuration\Validation()
        ]);
        return $this;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function setProxy(Configuration\Proxy $proxy): self
    {
        $this->proxy = $proxy;
        return $this;
    }
}
