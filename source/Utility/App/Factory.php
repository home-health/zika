<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Utility\App;

use Slim\App;
use Slim\CallableResolver;
use Slim\Psr7\Factory\ResponseFactory;
use Slim\Routing\RouteResolver;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.0.0
 */
final class Factory
{
    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function create(): App
    {
        $callable = new CallableResolver();
        $response = new ResponseFactory();
        $assemble = new Route\Collector($response, $callable);
        return (
            new Configuration(
                new Configuration\Proxy(
                    new App(
                        $response,
                        null,
                        $callable,
                        $assemble,
                        new RouteResolver(
                            $assemble,
                            new Route\Dispatcher($assemble)
                        )
                    )
                )
            )
        )->perform()->getProxy()->getApp();
    }
}
