<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Utility\App\Route;

use FastRoute\RouteCollector;
use Slim\Interfaces\DispatcherInterface;
use Slim\Interfaces\RouteCollectorInterface;
use Slim\Routing\FastRouteDispatcher;
use Slim\Routing\RoutingResults;

use function FastRoute\simpleDispatcher;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.6.0
 */
final class Dispatcher implements DispatcherInterface
{
    private $collector;
    private $processor;

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.6.0
     */
    public function __construct(RouteCollectorInterface $collector)
    {
        $this->setCollector($collector);
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.6.0
     */
    public function dispatch(string $method, string $uri): RoutingResults
    {
        $parameters = $this->getProcessor()->dispatch($method, $uri);
        return new RoutingResults($this, $method, $uri, $parameters[0], $parameters[1], $parameters[2]);
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.6.0
     */
    public function getAllowedMethods(string $uri): array
    {
        return $this->getProcessor()->getAllowedMethods($uri);
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.6.0
     */
    public function getCollector(): RouteCollectorInterface
    {
        return $this->collector;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.6.0
     */
    private function getProcessor(): FastRouteDispatcher
    {
        if ($this->processor === null) {
            $this->processor = simpleDispatcher(
                function (RouteCollector $collector) {
                    foreach ($this->getCollector()->getRoutes() as $route) {
                        $collector->addRoute(
                            $route->getMethods(),
                            $this->getCollector()->getBasePath() . $route->getPattern(),
                            $route->getIdentifier()
                        );
                    }
                },
                [
                    'dispatcher'  => FastRouteDispatcher::class,
                    'routeParser' => $this->getCollector()->getRouteParser()->getParser()
                ]
            );
        }
        return $this->processor;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.6.0
     */
    public function setCollector(RouteCollectorInterface $collector): self
    {
        $this->collector = $collector;
        return $this;
    }
}
