<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Utility\App\Route;

use FastRoute\RouteParser;
use InvalidArgumentException;
use Psr\Http\Message\UriInterface;
use Slim\Interfaces\RouteCollectorInterface;
use Slim\Interfaces\RouteParserInterface;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.6.0
 */
final class Processor implements RouteParserInterface
{
    private $collector;
    private $parser;

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.6.0
     */
    public function __construct(RouteCollectorInterface $collector, RouteParser $parser)
    {
        $this->setCollector($collector)->setParser($parser);
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.6.0
     */
    public function fullUrlFor(UriInterface $uri, string $name, array $data = [], array $parameters = []): string
    {
        $scheme = $uri->getScheme();
        $domain = $uri->getAuthority();
        $prefix = ($scheme ? $scheme . ':' : '') . ($domain ? '//' . $domain : '');
        return $prefix . $this->urlFor($name, $data, $parameters);
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.6.0
     */
    public function getCollector(): RouteCollectorInterface
    {
        return $this->collector;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.6.0
     */
    public function getParser(): RouteParser
    {
        return $this->parser;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.6.0
     */
    public function relativeUrlFor(string $name, array $data = [], array $parameters = []): string
    {
        $register = '';
        $sections = array_reverse($this->getParser()->parse($this->getCollector()->getNamedRoute($name)->getPattern()));
        $segments = [];
        foreach ($sections as $section) {
            foreach ($section as $segment) {
                if (is_string($segment)) {
                    $segments[] = $segment;
                    continue;
                }
                if (!array_key_exists($segment[0], $data)) {
                    $register = $segment[0];
                    $segments = [];
                    break;
                }
                $segments[] = $data[$segment[0]];
            }
            if (!empty($segments)) {
                break;
            }
        }
        if (empty($segments)) {
            throw new InvalidArgumentException("Missing data for URL segment: {$register}");
        }
        return $parameters ? implode('', $segments) . '?' . http_build_query($parameters) : implode('', $segments);
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.6.0
     */
    public function setCollector(RouteCollectorInterface $collector): self
    {
        $this->collector = $collector;
        return $this;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.6.0
     */
    public function setParser(RouteParser $parser): self
    {
        $this->parser = $parser;
        return $this;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.6.0
     */
    public function urlFor(string $name, array $data = [], array $parameters = []): string
    {
        $base = $this->getCollector()->getBasePath();
        $path = $this->relativeUrlFor($name, $data, $parameters);
        return $base !== null ? $base . $path : $path;
    }
}
