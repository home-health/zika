<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Utility\App\Route;

use HomeHealth\Zika\Handler\Strategy;
use Psr\Http\Message\ResponseFactoryInterface;
use Slim\Interfaces\CallableResolverInterface;
use Slim\Routing\RouteCollector;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.6.0
 */
final class Collector extends RouteCollector
{
    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.6.0
     */
    public function __construct(ResponseFactoryInterface $response, CallableResolverInterface $callable)
    {
        parent::__construct(
            $response,
            $callable,
            null,
            new Strategy\Containerization(),
            new Processor(
                $this,
                new Parser()
            )
        );
    }
}
