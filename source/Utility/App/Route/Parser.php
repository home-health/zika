<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Utility\App\Route;

use FastRoute\RouteParser;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.6.0
 */
final class Parser implements RouteParser
{
    private const DISPATCH_PATTERN = '[^/]+';
    private const VARIABLE_PATTERN = <<<'REGEX'
    \{
        \s* ([a-zA-Z._][a-zA-Z0-9._-]*) \s*
        (?:
            : \s* ([^{}]*(?:\{(?-1)\}[^{}]*)*)
        )?
    \}
    REGEX;

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.6.0
     */
    public function parse($pattern): array
    {
        $compound = '';
        $response = [];
        $sections = preg_split('~' . self::VARIABLE_PATTERN . '(*SKIP)(*F) | \[~x', rtrim($pattern, ']'));
        foreach ($sections as $section) {
            $compound .= $section;
            $response[] = $this->segregate($compound);
        }
        return $response;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.6.0
     */
    private function segregate(string $compound): array
    {
        $scission = preg_match_all(
            '~' . self::VARIABLE_PATTERN . '~x',
            $compound,
            $sections,
            PREG_OFFSET_CAPTURE | PREG_SET_ORDER
        );
        if ($scission) {
            $movement = 0;
            $response = [];
            foreach ($sections as $section) {
                if ($section[0][1] > $movement) {
                    $response[] = substr($compound, $movement, $section[0][1] - $movement);
                }
                $response[] = [
                    $section[1][0],
                    isset($section[2]) ? trim($section[2][0]) : self::DISPATCH_PATTERN
                ];
                $movement = $section[0][1] + strlen($section[0][0]);
            }
            if ($movement !== strlen($compound)) {
                $response[] = substr($compound, $movement);
            }
            return $response;
        }
        return [
            $compound
        ];
    }
}
