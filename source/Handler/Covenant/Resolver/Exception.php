<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Handler\Covenant\Resolver;

use HomeHealth\Zika\Handler\Covenant;
use Throwable;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.0.0
 */
interface Exception extends Covenant\Resolver
{
    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function knows(Throwable $throwable): bool;
}
