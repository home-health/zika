<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Handler\Resolver\Account;

use HomeHealth\Zika\Handler\Covenant;
use HomeHealth\Zika\Handler\Strategy\Containerization\Container;
use HomeHealth\Zika\Model;
use HomeHealth\Zika\Utility\Model\Encoder;
use HomeHealth\Zika\Utility\Model\Parser;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.0.0
 */
final class Create implements Covenant\Resolver
{
    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function __invoke(Container $container): ResponseInterface
    {
        $archive = $this->assemble($this->decode($container->getRequest()->getBody()));
        $archive->save();
        return $container->getResponse()->withStatus(201)->withHeader(
            'Location',
            "{$container->getRequest()->getUri()}/{$archive->getIdentifier()}"
        );
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    private function assemble(array $array): Model\Account
    {
        return (new Parser\Record())->assemble($array, Model\Account::class);
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    private function decode(StreamInterface $stream): array
    {
        return (new Encoder\JSON())->decode(strval($stream));
    }
}
