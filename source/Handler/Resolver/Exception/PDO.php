<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Handler\Resolver\Exception;

use HomeHealth\Zika\Handler\Covenant;
use HomeHealth\Zika\Handler\Strategy\Containerization\Container;
use PDOException;
use Psr\Http\Message\ResponseInterface;
use Throwable;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.0.0
 */
final class PDO implements Covenant\Resolver\Exception
{
    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function __invoke(Container $container): ResponseInterface
    {
        return $container->getResponse()->withStatus(
            $this->convert($container->getRequest()->getAttribute('throwable')->errorInfo[1])
        );
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @link            https://dev.mysql.com/doc/refman/8.0/en/server-error-reference.html
     * @since           1.0.0
     */
    private function convert(int $code): int
    {
        switch ($code) {
            case 1062:
                return 409;
            default:
                return 500;
        }
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function knows(Throwable $throwable): bool
    {
        return $throwable instanceof PDOException;
    }
}
