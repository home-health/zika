<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Handler\Resolver\Exception;

use HomeHealth\Zika\Handler\Covenant;
use HomeHealth\Zika\Handler\Strategy\Containerization\Container;
use HomeHealth\Zika\Utility\Model\Encoder\JWT\Exception;
use Psr\Http\Message\ResponseInterface;
use Throwable;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.0.0
 */
final class JWT implements Covenant\Resolver\Exception
{
    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function __invoke(Container $container): ResponseInterface
    {
        return $container->getResponse()->withStatus(401)->withHeader(
            'WWW-Authenticate',
            implode(', ', $this->encode(
                $container->getRequest()->hasHeader('Authorization'),
                $container->getRequest()->getAttribute('throwable')
            ))
        );
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    private function encode(bool $specified, Throwable $throwable): array
    {
        return array_merge([
            'Bearer realm="Interface for information exchange and storage"'
        ], $specified ? [
            'error="invalid_token"',
            "error_description=\"{$throwable->getMessage()}\""
        ] : []);
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function knows(Throwable $throwable): bool
    {
        return $throwable instanceof Exception;
    }
}
