<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Handler\Resolver\Exception;

use HomeHealth\Zika\Handler\Covenant;
use HomeHealth\Zika\Handler\Strategy\Containerization\Container;
use Psr\Http\Message\ResponseInterface;
use Slim\Exception\HttpException;
use Throwable;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.0.0
 */
final class HTTP implements Covenant\Resolver\Exception
{
    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function __invoke(Container $container): ResponseInterface
    {
        return $container->getResponse()->withStatus($container->getRequest()->getAttribute('throwable')->getCode());
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function knows(Throwable $throwable): bool
    {
        return $throwable instanceof HttpException;
    }
}
