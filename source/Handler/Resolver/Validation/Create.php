<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Handler\Resolver\Validation;

use DateTime;
use HomeHealth\Zika\Handler\Covenant;
use HomeHealth\Zika\Handler\Strategy\Containerization\Container;
use HomeHealth\Zika\Model;
use HomeHealth\Zika\Utility\Email;
use HomeHealth\Zika\Utility\Model\Encoder;
use HomeHealth\Zika\Utility\Model\Parser;
use HomeHealth\Zika\Utility\Translation;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;
use Symfony\Component\Mime;
use Symfony\Component\Translation\Translator;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.0.0
 */
final class Create implements Covenant\Resolver
{
    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function __invoke(Container $container): ResponseInterface
    {
        $expected = $this->assemble($this->decode($container->getRequest()->getBody()));
        $obtained = $this->query($expected)->findOne();
        if ($obtained !== null) {
            $validation = $this->create();
            $dispatcher = (new Email\Sender\Factory())->create();
            $translator = (new Translation\Agent\Factory())->create($obtained->getDisplays()->getLast()->getLanguage());
            $obtained->addValidation($validation)->save();
            $dispatcher->send($this->encode($translator, $validation));
            return $container->getResponse()->withStatus(201)->withHeader(
                'Location',
                "{$container->getRequest()->getUri()}/{$validation->getIdentifier()}"
            );
        }
        return $container->getResponse()->withStatus(403);
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    private function assemble(array $array): Model\Account
    {
        return (new Parser\Record())->assemble($array, Model\Account::class);
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    private function create(): Model\Validation
    {
        return (new Model\Validation())->setCreatedAt(
            new DateTime()
        )->setUpdatedAt(
            new DateTime()
        );
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    private function decode(StreamInterface $stream): array
    {
        return (new Encoder\JSON())->decode(strval($stream));
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    private function encode(Translator $translator, Model\Validation $validation): Mime\Email
    {
        return (new Email\Message\Factory())->create()->to(
            $validation->getAccount()->getEmailAddress()
        )->subject(
            $translator->trans('validation.credential.generated')
        )->text(
            $validation->getCredential()
        );
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    private function query(Model\Account $account): Model\AccountQuery
    {
        return Model\AccountQuery::create()->filterByEmailAddress($account->getEmailAddress());
    }
}
