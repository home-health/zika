<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Handler\Resolver\Session;

use DateTime;
use HomeHealth\Zika\Handler\Covenant;
use HomeHealth\Zika\Handler\Strategy\Containerization\Container;
use HomeHealth\Zika\Model;
use Psr\Http\Message\ResponseInterface;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.0.0
 */
final class Update implements Covenant\Resolver
{
    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function __invoke(Container $container): ResponseInterface
    {
        $criteria = $container->getRequest()->getAttribute('criteria');
        $material = $this->build($criteria);
        if ($criteria->update($material, null, true) > 0) {
            return $container->getResponse()->withStatus(204);
        }
        return $container->getResponse()->withStatus(404);
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    private function build(Model\RecordQuery $criteria): array
    {
        return [
            $criteria->getTableMap()->translateFieldName(
                $criteria->getTableMap()::COL_UPDATED_AT,
                $criteria->getTableMap()::TYPE_COLNAME,
                $criteria->getTableMap()::TYPE_FIELDNAME
            ) => new DateTime()
        ];
    }
}
