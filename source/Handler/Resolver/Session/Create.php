<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Handler\Resolver\Session;

use DateTime;
use HomeHealth\Zika\Handler\Covenant;
use HomeHealth\Zika\Handler\Strategy\Containerization\Container;
use HomeHealth\Zika\Model;
use HomeHealth\Zika\Utility\Model\Encoder;
use HomeHealth\Zika\Utility\Model\Parser;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\StreamInterface;
use Slim\Psr7\Factory\StreamFactory;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.0.0
 */
final class Create implements Covenant\Resolver
{
    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function __invoke(Container $container): ResponseInterface
    {
        $expected = $this->assemble($this->decode($container->getRequest()->getBody()));
        $obtained = $this->query($expected)->findOne();
        if ($obtained !== null && $this->grant($expected, $obtained)) {
            $tracing = $this->trace($container->getRequest());
            $session = $this->create();
            $obtained->addAccess($tracing)->addSession($session)->save();
            return $container->getResponse()->withStatus(201)->withBody(
                $this->encode($this->dismount($session))
            )->withHeader(
                'Location',
                "{$container->getRequest()->getUri()}/{$session->getIdentifier()}"
            );
        }
        return $container->getResponse()->withStatus(403);
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    private function assemble(array $array): Model\Account
    {
        return (new Parser\Record())->assemble($array, Model\Account::class);
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    private function create(): Model\Session
    {
        return (new Model\Session())->setCreatedAt(
            new DateTime()
        )->setUpdatedAt(
            new DateTime()
        );
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    private function decode(StreamInterface $stream): array
    {
        return (new Encoder\JSON())->decode(strval($stream));
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    private function dismount(Model\Session $session): array
    {
        return (new Parser\Record())->dismount($session);
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    private function encode(array $array): StreamInterface
    {
        return (new StreamFactory())->createStream((new Encoder\JSON())->encode($array));
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    private function grant(Model\Account $expected, Model\Account $obtained): bool
    {
        return password_verify($expected->getPassword(), $obtained->getPassword());
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    private function query(Model\Account $account): Model\AccountQuery
    {
        return Model\AccountQuery::create()->filterByEmailAddress($account->getEmailAddress());
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    private function trace(ServerRequestInterface $request): Model\Access
    {
        return (new Model\Access())->setBrowser(
            $request->getAttribute('browser')
        )->setIPAddress(
            $request->getAttribute('ip_address')
        )->setSystem(
            $request->getAttribute('system')
        );
    }
}
