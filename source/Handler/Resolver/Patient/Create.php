<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Handler\Resolver\Patient;

use HomeHealth\Zika\Handler\Covenant;
use HomeHealth\Zika\Handler\Strategy\Containerization\Container;
use HomeHealth\Zika\Model;
use HomeHealth\Zika\Utility\Model\Encoder;
use HomeHealth\Zika\Utility\Model\Parser;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.2.0
 */
final class Create implements Covenant\Resolver
{
    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.2.0
     */
    public function __invoke(Container $container): ResponseInterface
    {
        $session = $container->getRequest()->getAttribute('session');
        $archive = $this->contain($this->assemble($this->decode($container->getRequest()->getBody())), $session);
        $archive->save();
        return $container->getResponse()->withStatus(201)->withHeader(
            'Location',
            "{$container->getRequest()->getUri()}/{$archive->getIdentifier()}"
        );
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.2.0
     */
    private function assemble(array $array): Model\Patient
    {
        return (new Parser\Record())->assemble($array, Model\Patient::class);
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.2.0
     */
    private function contain(Model\Patient $patient, Model\Session $session): Model\Patient
    {
        return $patient->setAccountIdentifier($session->getAccountIdentifier());
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.2.0
     */
    private function decode(StreamInterface $stream): array
    {
        return (new Encoder\JSON())->decode(strval($stream));
    }
}
