<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Handler\Resolver\Access;

use HomeHealth\Zika\Handler\Covenant;
use HomeHealth\Zika\Handler\Strategy\Containerization\Container;
use Psr\Http\Message\ResponseInterface;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.0.0
 */
final class Delete implements Covenant\Resolver
{
    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function __invoke(Container $container): ResponseInterface
    {
        if ($container->getRequest()->getAttribute('criteria')->delete() > 0) {
            return $container->getResponse()->withStatus(204);
        }
        return $container->getResponse()->withStatus(404);
    }
}
