<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Handler\Resolver\PatientDocument;

use HomeHealth\Zika\Handler\Covenant;
use HomeHealth\Zika\Handler\Strategy\Containerization\Container;
use HomeHealth\Zika\Model;
use Psr\Http\Message\ResponseInterface;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.5.0
 */
final class Delete implements Covenant\Resolver
{
    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.5.0
     */
    public function __invoke(Container $container): ResponseInterface
    {
        $criteria = $container->getRequest()->getAttribute('criteria');
        if ($criteria->exists() && $this->segregate($criteria)->delete() > 0) {
            return $container->getResponse()->withStatus(204);
        }
        return $container->getResponse()->withStatus(404);
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.6.0
     */
    private function segregate(Model\RecordQuery $criteria): Model\RecordQuery
    {
        $resolution = new Model\PatientDocumentQuery();
        $criterions = array_intersect_key(
            $criteria->getMap(),
            array_flip(
                $resolution->getTableMap()->getFieldNames(
                    $resolution->getTableMap()::TYPE_COLNAME
                )
            )
        );
        foreach ($criterions as $criterion) {
            $resolution->add($criterion);
        }
        return $resolution;
    }
}
