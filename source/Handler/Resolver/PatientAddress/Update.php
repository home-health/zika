<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Handler\Resolver\PatientAddress;

use HomeHealth\Zika\Handler\Covenant;
use HomeHealth\Zika\Handler\Strategy\Containerization\Container;
use HomeHealth\Zika\Model;
use HomeHealth\Zika\Utility\Model\Encoder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.3.0
 */
final class Update implements Covenant\Resolver
{
    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.3.0
     */
    public function __invoke(Container $container): ResponseInterface
    {
        $criteria = $container->getRequest()->getAttribute('criteria');
        if ($criteria->exists()) {
            $material = $this->decode($container->getRequest()->getBody());
            if ($this->segregate($criteria)->update($material, null, true) > 0) {
                return $container->getResponse()->withStatus(204);
            }
        }
        return $container->getResponse()->withStatus(404);
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.3.0
     */
    private function decode(StreamInterface $stream): array
    {
        return (new Encoder\JSON())->decode(strval($stream));
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.6.0
     */
    private function segregate(Model\RecordQuery $criteria): Model\RecordQuery
    {
        $resolution = new Model\PatientAddressQuery();
        $criterions = array_intersect_key(
            $criteria->getMap(),
            array_flip(
                $resolution->getTableMap()->getFieldNames(
                    $resolution->getTableMap()::TYPE_COLNAME
                )
            )
        );
        foreach ($criterions as $criterion) {
            $resolution->add($criterion);
        }
        return $resolution;
    }
}
