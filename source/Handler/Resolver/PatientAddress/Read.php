<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Handler\Resolver\PatientAddress;

use HomeHealth\Zika\Handler\Covenant;
use HomeHealth\Zika\Handler\Strategy\Containerization\Container;
use HomeHealth\Zika\Model;
use HomeHealth\Zika\Utility\Model\Encoder;
use HomeHealth\Zika\Utility\Model\Parser;
use Propel\Runtime\Collection\Collection;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;
use Slim\Psr7\Factory\StreamFactory;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.3.0
 */
final class Read implements Covenant\Resolver
{
    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.3.0
     */
    public function __invoke(Container $container): ResponseInterface
    {
        $criteria = $container->getRequest()->getAttribute('criteria');
        if ($this->segregate($criteria)->exists()) {
            $collection = $criteria->find();
            $restricted = $container->hasArgument($collection->getTableMapClass()::COL_IDENTIFIER);
            if ($collection->count() > 0) {
                return $container->getResponse()->withBody($this->encode($this->dismount($collection, $restricted)));
            }
            return $container->getResponse()->withStatus($restricted ? 404 : 204);
        }
        return $container->getResponse()->withStatus(404);
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.3.0
     */
    private function dismount(Collection $collection, bool $extract): array
    {
        $array = (new Parser\Collection\Record())->dismount($collection);
        return $extract ? array_shift($array) : $array;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.3.0
     */
    private function encode(array $array): StreamInterface
    {
        return (new StreamFactory())->createStream((new Encoder\JSON())->encode($array));
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.6.0
     */
    private function segregate(Model\RecordQuery $criteria): Model\RecordQuery
    {
        $resolution = new Model\PatientQuery();
        $criterions = array_intersect_key(
            $criteria->getMap(),
            array_flip(
                $resolution->getTableMap()->getFieldNames(
                    $resolution->getTableMap()::TYPE_COLNAME
                )
            )
        );
        foreach ($criterions as $criterion) {
            $resolution->add($criterion);
        }
        return $resolution;
    }
}
