<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Handler\Resolver\PatientAddress;

use HomeHealth\Zika\Handler\Covenant;
use HomeHealth\Zika\Handler\Strategy\Containerization\Container;
use HomeHealth\Zika\Model;
use HomeHealth\Zika\Utility\Model\Encoder;
use HomeHealth\Zika\Utility\Model\Parser;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.3.0
 */
final class Create implements Covenant\Resolver
{
    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.3.0
     */
    public function __invoke(Container $container): ResponseInterface
    {
        $criteria = $container->getRequest()->getAttribute('criteria');
        if ($criteria->exists()) {
            $limiter = $container->getArgument(Model\Map\PatientTableMap::COL_IDENTIFIER);
            $archive = $this->contain($this->assemble($this->decode($container->getRequest()->getBody())), $limiter);
            $archive->save();
            return $container->getResponse()->withStatus(201)->withHeader(
                'Location',
                "{$container->getRequest()->getUri()}/{$archive->getIdentifier()}"
            );
        }
        return $container->getResponse()->withStatus(404);
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.3.0
     */
    private function assemble(array $array): Model\PatientAddress
    {
        return (new Parser\Record())->assemble($array, Model\PatientAddress::class);
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.3.0
     */
    private function contain(Model\PatientAddress $address, string $patient_identifier): Model\PatientAddress
    {
        return $address->setPatientIdentifier($patient_identifier);
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.3.0
     */
    private function decode(StreamInterface $stream): array
    {
        return (new Encoder\JSON())->decode(strval($stream));
    }
}
