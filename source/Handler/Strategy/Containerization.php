<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Handler\Strategy;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Interfaces\InvocationStrategyInterface;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.0.0
 */
final class Containerization implements InvocationStrategyInterface
{
    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function __invoke(callable $resolver, Request $request, Response $response, array $arguments): Response
    {
        return $resolver(new Containerization\Container($arguments, $request, $response));
    }
}
