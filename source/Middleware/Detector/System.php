<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Middleware\Detector;

use DeviceDetector\Parser;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.0.0
 */
final class System implements MiddlewareInterface
{
    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    private function encode(array $array): ?string
    {
        if (count($array) > 0) {
            return implode(' ', array_filter(array_intersect_key($array, [
                'name'     => null,
                'version'  => null,
                'platform' => null
            ]), 'strlen'));
        }
        return null;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    private function dismount(ServerRequestInterface $request): array
    {
        Parser\ParserAbstract::setVersionTruncation(Parser\ParserAbstract::VERSION_TRUNCATION_NONE);
        return (new Parser\OperatingSystem($request->getHeaderLine('User-Agent')))->parse();
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        return $handler->handle($request->withAttribute('system', $this->encode($this->dismount($request))));
    }
}
