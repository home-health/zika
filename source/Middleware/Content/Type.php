<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Middleware\Content;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.0.0
 */
final class Type implements MiddlewareInterface
{
    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        if (!($response = $handler->handle($request))->hasHeader('Content-Type')) {
            return $response->withHeader('Content-Type', 'application/json');
        }
        return $response;
    }
}
