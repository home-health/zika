<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Middleware;

use DateTime;
use HomeHealth\Zika\Model;
use HomeHealth\Zika\Utility\Model\Encoder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.0.0
 */
final class Session implements MiddlewareInterface
{
    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    private function assemble(array $array): Model\Session
    {
        $session = new Model\Session();
        $session->fromArray(array_values($array), Model\Map\SessionTableMap::TYPE_NUM);
        $session->setNew(false);
        return $session;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    private function decode(string $string): array
    {
        return (new Encoder\JWT(getenv('JWT_SECRET_KEY_SESSION')))->decode(str_replace('Bearer ', '', $string));
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        return $handler->handle($request->withAttribute(
            'session',
            $this->assemble($this->decode($request->getHeaderLine('Authorization')))
        ));
    }
}
