<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Middleware;

use HomeHealth\Zika\Handler\Strategy\Containerization\Container;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Slim\Psr7\Response;
use Throwable;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.0.0
 */
final class Exception implements MiddlewareInterface
{
    private $resolvers;

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function __construct(array $resolvers)
    {
        $this->setResolvers($resolvers);
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    private function create(ServerRequestInterface $request, Throwable $throwable): Container
    {
        return new Container([], $request->withAttribute('throwable', $throwable), new Response());
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    private function extract(Throwable $throwable): Throwable
    {
        if ($throwable->getPrevious() !== null) {
            return $this->extract($throwable->getPrevious());
        }
        return $throwable;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    private function forward(ServerRequestInterface $request, Throwable $throwable): ResponseInterface
    {
        $throwable = $this->extract($throwable);
        foreach ($this->getResolvers() as $resolver) {
            if ($resolver->knows($throwable)) {
                return $resolver($this->create($request, $throwable));
            }
        }
        return (new Response())->withStatus(500);
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function getResolvers(): array
    {
        return $this->resolvers;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        try {
            return $handler->handle($request);
        } catch (Throwable $throwable) {
            error_log(strval($throwable));
            return $this->forward($request, $throwable);
        }
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function setResolvers(array $resolvers): self
    {
        $this->resolvers = $resolvers;
        return $this;
    }
}
