<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Slim\Interfaces\RouteResolverInterface;
use Slim\Psr7\Response;
use Slim\Routing\RoutingResults;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.0.0
 */
final class CORS implements MiddlewareInterface
{
    private $resolver;

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function __construct(RouteResolverInterface $resolver)
    {
        $this->setResolver($resolver);
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    private function compute(string $method, string $path): RoutingResults
    {
        return $this->getResolver()->computeRoutingResults($path, $method);
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function getResolver(): RouteResolverInterface
    {
        return $this->resolver;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    private function perform(RequestHandlerInterface $handler, ServerRequestInterface $request): ResponseInterface
    {
        if ($request->getMethod() === 'OPTIONS') {
            return new Response();
        }
        return $handler->handle($request);
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        return $this->perform($handler, $request)->withHeader(
            'Access-Control-Allow-Headers',
            'Authorization, Content-Type'
        )->withHeader(
            'Access-Control-Allow-Methods',
            implode(', ', $this->compute($request->getMethod(), $request->getUri()->getPath())->getAllowedMethods())
        )->withHeader(
            'Access-Control-Allow-Origin',
            '*'
        );
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function setResolver(RouteResolverInterface $resolver)
    {
        $this->resolver = $resolver;
        return $this;
    }
}
