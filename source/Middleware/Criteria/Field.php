<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Middleware\Criteria;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.6.0
 */
final class Field implements MiddlewareInterface
{
    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.6.0
     */
    private function getParameter(ServerRequestInterface $request): array
    {
        $parameters = $request->getQueryParams();
        return isset($parameters['fields']) && is_array($parameters['fields']) ? $parameters['fields'] : [];
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.6.0
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $attribute = $request->getAttribute('criteria');
        $potential = $attribute->getTableMap()->getFieldnames($attribute->getTableMap()::TYPE_FIELDNAME);
        $parameter = $this->getParameter($request);
        $intersect = array_intersect($potential, $parameter);
        if (count($intersect) > 0) {
            $request = $request->withAttribute('criteria', $attribute->select($intersect));
        }
        return $handler->handle($request);
    }
}
