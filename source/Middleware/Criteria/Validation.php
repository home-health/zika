<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Middleware\Criteria;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.6.0
 */
final class Validation implements MiddlewareInterface
{
    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.6.0
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $attribute = $request->getAttribute('criteria');
        $parameter = $request->getAttribute('validation');
        return $handler->handle($request->withAttribute('criteria', $attribute->filterByValidation($parameter)));
    }
}
