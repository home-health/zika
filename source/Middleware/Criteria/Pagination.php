<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Middleware\Criteria;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.7.0
 */
final class Pagination implements MiddlewareInterface
{
    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.7.0
     */
    private function getParameter(ServerRequestInterface $request): int
    {
        $parameters = $request->getQueryParams();
        return isset($parameters['page']) && is_numeric($parameters['page']) ? intval($parameters['page']) : 0;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.7.0
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $attribute = $request->getAttribute('criteria');
        $parameter = $this->getParameter($request);
        return $handler->handle($request->withAttribute('criteria', $attribute->limit(20)->offset($parameter * 20)));
    }
}
