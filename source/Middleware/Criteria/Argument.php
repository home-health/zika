<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Middleware\Criteria;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.6.0
 */
final class Argument implements MiddlewareInterface
{
    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.6.0
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $attribute = $request->getAttribute('criteria');
        $arguments = $request->getAttribute('__route__')->getArguments();
        foreach ($arguments as $field => $value) {
            $request = $request->withAttribute('criteria', $attribute->addUsingAlias($field, $value));
        }
        return $handler->handle($request);
    }
}
