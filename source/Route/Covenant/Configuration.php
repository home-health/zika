<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Route\Covenant;

use Slim\Interfaces\RouteCollectorProxyInterface;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.0.0
 */
interface Configuration
{
    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function __invoke(RouteCollectorProxyInterface $collector): void;
}
