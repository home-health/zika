<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Route\Configuration;

use HomeHealth\Zika\Handler\Resolver;
use HomeHealth\Zika\Middleware;
use HomeHealth\Zika\Model;
use HomeHealth\Zika\Route\Covenant;
use Slim\Interfaces\RouteCollectorProxyInterface;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.0.0
 */
final class Display implements Covenant\Configuration
{
    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function __invoke(RouteCollectorProxyInterface $route): void
    {
        $route->group(
            '/displays',
            function (RouteCollectorProxyInterface $route) {
                $route->group(
                    '/{' . Model\Map\DisplayTableMap::COL_IDENTIFIER . ':\d+}',
                    function (RouteCollectorProxyInterface $route) {
                        $route->get(
                            '',
                            Resolver\Display\Read::class
                        )->add(
                            new Middleware\Criteria\Field()
                        );
                        $route->patch(
                            '',
                            Resolver\Display\Update::class
                        );
                    }
                );
                $route->get(
                    '',
                    Resolver\Display\Read::class
                )->add(
                    new Middleware\Criteria\Pagination()
                )->add(
                    new Middleware\Criteria\Field()
                );
            }
        )->add(
            new Middleware\Criteria\Argument()
        )->add(
            new Middleware\Criteria\Session()
        )->add(
            new Middleware\Criteria(
                new Model\DisplayQuery()
            )
        )->add(
            new Middleware\Session()
        );
    }
}
