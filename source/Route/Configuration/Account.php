<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Route\Configuration;

use HomeHealth\Zika\Handler\Resolver;
use HomeHealth\Zika\Middleware;
use HomeHealth\Zika\Model;
use HomeHealth\Zika\Route\Covenant;
use Slim\Interfaces\RouteCollectorProxyInterface;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.0.0
 */
final class Account implements Covenant\Configuration
{
    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function __invoke(RouteCollectorProxyInterface $route): void
    {
        $route->group(
            '/accounts',
            function (RouteCollectorProxyInterface $route) {
                $route->group(
                    '/{' . Model\Map\AccountTableMap::COL_IDENTIFIER . ':\d+}',
                    function (RouteCollectorProxyInterface $route) {
                        $route->patch(
                            '',
                            Resolver\Account\Update::class
                        );
                        $route->delete(
                            '',
                            Resolver\Account\Delete::class
                        );
                    }
                )->add(
                    new Middleware\Criteria\Argument()
                )->add(
                    new Middleware\Criteria\Validation()
                )->add(
                    new Middleware\Criteria(
                        new Model\AccountQuery()
                    )
                )->add(
                    new Middleware\Validation()
                );
                $route->group(
                    '',
                    function (RouteCollectorProxyInterface $route) {
                        $route->get(
                            '/{' . Model\Map\AccountTableMap::COL_IDENTIFIER . ':\d+}',
                            Resolver\Account\Read::class
                        )->add(
                            new Middleware\Criteria\Argument()
                        );
                        $route->get(
                            '',
                            Resolver\Account\Read::class
                        )->add(
                            new Middleware\Criteria\Pagination()
                        );
                    }
                )->add(
                    new Middleware\Criteria\Field()
                )->add(
                    new Middleware\Criteria\Session()
                )->add(
                    new Middleware\Criteria(
                        new Model\AccountQuery()
                    )
                )->add(
                    new Middleware\Session()
                );
                $route->post(
                    '',
                    Resolver\Account\Create::class
                );
            }
        );
    }
}
