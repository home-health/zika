<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Route\Configuration;

use HomeHealth\Zika\Handler\Resolver;
use HomeHealth\Zika\Middleware;
use HomeHealth\Zika\Model;
use HomeHealth\Zika\Route\Covenant;
use Slim\Interfaces\RouteCollectorProxyInterface;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.0.0
 */
final class Access implements Covenant\Configuration
{
    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function __invoke(RouteCollectorProxyInterface $route): void
    {
        $route->group(
            '/accesses',
            function (RouteCollectorProxyInterface $route) {
                $route->group(
                    '/{' . Model\Map\AccessTableMap::COL_IDENTIFIER . ':\d+}',
                    function (RouteCollectorProxyInterface $route) {
                        $route->get(
                            '',
                            Resolver\Access\Read::class
                        )->add(
                            new Middleware\Criteria\Field()
                        );
                        $route->delete(
                            '',
                            Resolver\Access\Delete::class
                        );
                    }
                );
                $route->get(
                    '',
                    Resolver\Access\Read::class
                )->add(
                    new Middleware\Criteria\Pagination()
                )->add(
                    new Middleware\Criteria\Field()
                );
            }
        )->add(
            new Middleware\Criteria\Argument()
        )->add(
            new Middleware\Criteria\Session()
        )->add(
            new Middleware\Criteria(
                new Model\AccessQuery()
            )
        )->add(
            new Middleware\Session()
        );
    }
}
