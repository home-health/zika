<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Route\Configuration;

use HomeHealth\Zika\Handler\Resolver;
use HomeHealth\Zika\Middleware;
use HomeHealth\Zika\Model;
use HomeHealth\Zika\Route\Covenant;
use Slim\Interfaces\RouteCollectorProxyInterface;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.0.0
 */
final class Session implements Covenant\Configuration
{
    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     */
    public function __invoke(RouteCollectorProxyInterface $route): void
    {
        $route->group(
            '/sessions',
            function (RouteCollectorProxyInterface $route) {
                $route->group(
                    '',
                    function (RouteCollectorProxyInterface $route) {
                        $route->group(
                            '/{' . Model\Map\SessionTableMap::COL_IDENTIFIER . ':\d+}',
                            function (RouteCollectorProxyInterface $route) {
                                $route->get(
                                    '',
                                    Resolver\Session\Read::class
                                )->add(
                                    new Middleware\Criteria\Field()
                                );
                                $route->put(
                                    '',
                                    Resolver\Session\Update::class
                                );
                                $route->delete(
                                    '',
                                    Resolver\Session\Delete::class
                                );
                            }
                        )->add(
                            new Middleware\Criteria\Argument()
                        );
                        $route->get(
                            '',
                            Resolver\Session\Read::class
                        )->add(
                            new Middleware\Criteria\Pagination()
                        )->add(
                            new Middleware\Criteria\Field()
                        );
                    }
                )->add(
                    new Middleware\Criteria\Session()
                )->add(
                    new Middleware\Criteria(
                        new Model\SessionQuery()
                    )
                )->add(
                    new Middleware\Session()
                );
                $route->post(
                    '',
                    Resolver\Session\Create::class
                )->add(
                    new Middleware\Detector\Browser()
                )->add(
                    new Middleware\Detector\IPAddress()
                )->add(
                    new Middleware\Detector\System()
                );
            }
        );
    }
}
