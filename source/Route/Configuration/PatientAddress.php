<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Route\Configuration;

use HomeHealth\Zika\Handler\Resolver;
use HomeHealth\Zika\Middleware;
use HomeHealth\Zika\Model;
use HomeHealth\Zika\Route\Covenant;
use Slim\Interfaces\RouteCollectorProxyInterface;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.3.0
 */
final class PatientAddress implements Covenant\Configuration
{
    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.3.0
     */
    public function __invoke(RouteCollectorProxyInterface $route): void
    {
        $route->group(
            '/patients/{' . Model\Map\PatientTableMap::COL_IDENTIFIER . ':\d+}/addresses',
            function (RouteCollectorProxyInterface $route) {
                $route->group(
                    '',
                    function (RouteCollectorProxyInterface $route) {
                        $route->group(
                            '/{' . Model\Map\PatientAddressTableMap::COL_IDENTIFIER . ':\d+}',
                            function (RouteCollectorProxyInterface $route) {
                                $route->get(
                                    '',
                                    Resolver\PatientAddress\Read::class
                                )->add(
                                    new Middleware\Criteria\Field()
                                );
                                $route->patch(
                                    '',
                                    Resolver\PatientAddress\Update::class
                                );
                                $route->delete(
                                    '',
                                    Resolver\PatientAddress\Delete::class
                                );
                            }
                        );
                        $route->get(
                            '',
                            Resolver\PatientAddress\Read::class
                        )->add(
                            new Middleware\Criteria\Pagination()
                        )->add(
                            new Middleware\Criteria\Field()
                        );
                    }
                )->add(
                    new Middleware\Criteria\Argument()
                )->add(
                    new Middleware\Criteria\Session()
                )->add(
                    new Middleware\Criteria(
                        new Model\PatientAddressQuery()
                    )
                );
                $route->post(
                    '',
                    Resolver\PatientAddress\Create::class
                )->add(
                    new Middleware\Criteria\Argument()
                )->add(
                    new Middleware\Criteria\Session()
                )->add(
                    new Middleware\Criteria(
                        new Model\PatientQuery()
                    )
                );
            }
        )->add(
            new Middleware\Session()
        );
    }
}
