<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Route\Configuration;

use HomeHealth\Zika\Handler\Resolver;
use HomeHealth\Zika\Middleware;
use HomeHealth\Zika\Model;
use HomeHealth\Zika\Route\Covenant;
use Slim\Interfaces\RouteCollectorProxyInterface;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.5.0
 */
final class PatientDocument implements Covenant\Configuration
{
    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.5.0
     */
    public function __invoke(RouteCollectorProxyInterface $route): void
    {
        $route->group(
            '/patients/{' . Model\Map\PatientTableMap::COL_IDENTIFIER . ':\d+}/documents',
            function (RouteCollectorProxyInterface $route) {
                $route->group(
                    '',
                    function (RouteCollectorProxyInterface $route) {
                        $route->group(
                            '/{' . Model\Map\PatientDocumentTableMap::COL_IDENTIFIER . ':\d+}',
                            function (RouteCollectorProxyInterface $route) {
                                $route->get(
                                    '',
                                    Resolver\PatientDocument\Read::class
                                )->add(
                                    new Middleware\Criteria\Field()
                                );
                                $route->patch(
                                    '',
                                    Resolver\PatientDocument\Update::class
                                );
                                $route->delete(
                                    '',
                                    Resolver\PatientDocument\Delete::class
                                );
                            }
                        );
                        $route->get(
                            '',
                            Resolver\PatientDocument\Read::class
                        )->add(
                            new Middleware\Criteria\Pagination()
                        )->add(
                            new Middleware\Criteria\Field()
                        );
                    }
                )->add(
                    new Middleware\Criteria\Argument()
                )->add(
                    new Middleware\Criteria\Session()
                )->add(
                    new Middleware\Criteria(
                        new Model\PatientDocumentQuery()
                    )
                );
                $route->post(
                    '',
                    Resolver\PatientDocument\Create::class
                )->add(
                    new Middleware\Criteria\Argument()
                )->add(
                    new Middleware\Criteria\Session()
                )->add(
                    new Middleware\Criteria(
                        new Model\PatientQuery()
                    )
                );
            }
        )->add(
            new Middleware\Session()
        );
    }
}
