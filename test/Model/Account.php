<?php

declare(strict_types=1);

namespace HomeHealth\Zika\Test\Model;

use DateTime;
use HomeHealth\Zika\Model;
use PHPUnit\Framework\TestCase;
use Propel\Runtime\Exception\PropelException;
use ReflectionClass;
use Throwable;

/**
 * @author              Gabriel Pereira de Barros <opereirabarros@outlook.com>
 * @copyright           © 2020 Gabriel Pereira de Barros. All Rights Reserved.
 * @since               1.0.0
 * @coversDefaultClass  HomeHealth\Zika\Model\Account
 */
final class Account extends TestCase
{
    private $expected;

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     * @before
     */
    public function __createExpected(): void
    {
        $expected = ($this->expected = new Model\Account());
        $expected->setCreatedAt(new DateTime('2020-02-12 14:40:00.000000'));
        $expected->setDeleted(false);
        $expected->setEmailAddress('test@test.com');
        $expected->setEmailVerified(true);
        $expected->setIdentifier(1);
        $expected->setNew(true);
        $expected->setPassword('12345678');
        $expected->setUpdatedAt(new DateTime('2020-02-12 14:50:00.000000'));
        $expected->setVirtualColumn(Model\Map\AccountTableMap::TABLE_NAME, 2);
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     * @test
     * @testdox         Expected
     * @group           generated
     * @coversNothing
     */
    public function __expected(): void
    {
        $expected = new DateTime('2020-02-12 14:40:00.000000');
        $obtained = $this->expected->getCreatedAt();
        $this->assertEquals($expected, $obtained);

        $expected = 'test@test.com';
        $obtained = $this->expected->getEmailAddress();
        $this->assertSame($expected, $obtained);

        $expected = true;
        $obtained = $this->expected->getEmailVerified();
        $this->assertSame($expected, $obtained);

        $expected = 1;
        $obtained = $this->expected->getIdentifier();
        $this->assertSame($expected, $obtained);

        $expected = '12345678';
        $obtained = $this->expected->getPassword();
        $this->assertSame($expected, $obtained);

        $expected = new DateTime('2020-02-12 14:50:00.000000');
        $obtained = $this->expected->getUpdatedAt();
        $this->assertEquals($expected, $obtained);

        $expected = 2;
        $obtained = $this->expected->getVirtualColumn(Model\Map\AccountTableMap::TABLE_NAME);
        $this->assertSame($expected, $obtained);

        $expected = false;
        $obtained = $this->expected->isDeleted();
        $this->assertSame($expected, $obtained);

        $expected = true;
        $obtained = $this->expected->isEmailVerified();
        $this->assertSame($expected, $obtained);

        $expected = true;
        $obtained = $this->expected->isNew();
        $this->assertSame($expected, $obtained);
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     * @after
     */
    public function __removeExpected(): void
    {
        $this->expected = null;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     * @test
     * @group           generated
     * @covers          ::applyDefaultValues
     * @depends         construct
     */
    public function applyDefaultValues(Model\Account $account): Model\Account
    {
        $expected = null;
        $obtained = $account->applyDefaultValues();
        $this->assertSame($expected, $obtained);

        return $account;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     * @test
     * @group           generated
     * @covers          ::clear
     * @depends         delete
     */
    public function clear(Model\Account $account): Model\Account
    {
        $expected = null;
        $obtained = $account->clear();
        $this->assertSame($expected, $obtained);

        $expected = null;
        $obtained = $account->getCreatedAt();
        $this->assertSame($expected, $obtained);

        $expected = null;
        $obtained = $account->getEmailAddress();
        $this->assertSame($expected, $obtained);

        $expected = false;
        $obtained = $account->getEmailVerified();
        $this->assertSame($expected, $obtained);

        $expected = null;
        $obtained = $account->getIdentifier();
        $this->assertSame($expected, $obtained);

        $expected = [];
        $obtained = $account->getModifiedColumns();
        $this->assertSame($expected, $obtained);

        $expected = null;
        $obtained = $account->getPassword();
        $this->assertSame($expected, $obtained);

        $expected = null;
        $obtained = $account->getUpdatedAt();
        $this->assertSame($expected, $obtained);

        $expected = false;
        $obtained = $this->expected->isDeleted();
        $this->assertSame($expected, $obtained);

        $expected = true;
        $obtained = $this->expected->isNew();
        $this->assertSame($expected, $obtained);

        return $account;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     * @test
     * @group           generated
     * @covers          ::clearAllReferences
     * @depends         delete
     */
    public function clearAllReferences(Model\Account $account): Model\Account
    {
        $expected = null;
        $obtained = $account->clearAllReferences();
        $this->assertSame($expected, $obtained);

        return $account;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     * @test
     * @group           generated
     * @covers          ::__construct
     */
    public function construct(): Model\Account
    {
        $expected = Model\Account::class;
        $obtained = new Model\Account();
        $this->assertInstanceOf($expected, $obtained);

        return $obtained;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     * @test
     * @group           generated
     * @covers          ::copy
     * @depends         setCreatedAt
     * @depends         setEmailAddress
     * @depends         setEmailVerified
     * @depends         setIdentifier
     * @depends         setPassword
     * @depends         setUpdatedAt
     */
    public function copy(Model\Account $account): Model\Account
    {
        $copy = $account->copy();

        $expected = Model\Account::class;
        $obtained = $copy;
        $this->assertInstanceOf($expected, $obtained);

        $expected = $account->getCreatedAt();
        $obtained = $copy->getCreatedAt();
        $this->assertEquals($expected, $obtained);

        $expected = $account->getEmailAddress();
        $obtained = $copy->getEmailAddress();
        $this->assertSame($expected, $obtained);

        $expected = $account->getEmailVerified();
        $obtained = $copy->getEmailVerified();
        $this->assertSame($expected, $obtained);

        $expected = null;
        $obtained = $copy->getIdentifier();
        $this->assertSame($expected, $obtained);

        $expected = $account->getPassword();
        $obtained = $copy->getPassword();
        $this->assertSame($expected, $obtained);

        $expected = $account->getUpdatedAt();
        $obtained = $copy->getUpdatedAt();
        $this->assertEquals($expected, $obtained);

        $expected = true;
        $obtained = $copy->isNew();
        $this->assertSame($expected, $obtained);

        return $account;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     * @test
     * @group           generated
     * @covers          ::copyInto
     * @depends         setCreatedAt
     * @depends         setEmailAddress
     * @depends         setEmailVerified
     * @depends         setIdentifier
     * @depends         setPassword
     * @depends         setUpdatedAt
     */
    public function copyInto(Model\Account $account): Model\Account
    {
        $account->copyInto($copy = new Model\Account());

        $expected = Model\Account::class;
        $obtained = $copy;
        $this->assertInstanceOf($expected, $obtained);

        $expected = $account->getCreatedAt();
        $obtained = $copy->getCreatedAt();
        $this->assertEquals($expected, $obtained);

        $expected = $account->getEmailAddress();
        $obtained = $copy->getEmailAddress();
        $this->assertSame($expected, $obtained);

        $expected = $account->getEmailVerified();
        $obtained = $copy->getEmailVerified();
        $this->assertSame($expected, $obtained);

        $expected = null;
        $obtained = $copy->getIdentifier();
        $this->assertSame($expected, $obtained);

        $expected = $account->getPassword();
        $obtained = $copy->getPassword();
        $this->assertSame($expected, $obtained);

        $expected = $account->getUpdatedAt();
        $obtained = $copy->getUpdatedAt();
        $this->assertEquals($expected, $obtained);

        $expected = true;
        $obtained = $copy->isNew();
        $this->assertSame($expected, $obtained);

        return $account;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     * @test
     * @group           generated
     * @covers          ::delete
     * @depends         preDelete
     */
    public function delete(Model\Account $account): Model\Account
    {
        $expected = null;
        $obtained = $account->delete();
        $this->assertSame($expected, $obtained);

        $expected = true;
        $obtained = $account->isDeleted();
        $this->assertSame($expected, $obtained);

        try {
            $account->delete();
        } catch (Throwable $throwable) {
            $expected = PropelException::class;
            $obtained = $throwable;
            $this->assertInstanceOf($expected, $obtained);
        }

        return $account;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     * @test
     * @group           generated
     * @covers          ::equals
     * @depends         setIdentifier
     */
    public function equals(Model\Account $account): Model\Account
    {
        $expected = false;
        $obtained = $account->equals(Model\Map\AccountTableMap::TABLE_NAME);
        $this->assertSame($expected, $obtained);

        $expected = true;
        $obtained = $account->equals($account);
        $this->assertSame($expected, $obtained);

        $expected = false;
        $obtained = $account->equals($this->expected->copy());
        $this->assertSame($expected, $obtained);

        $expected = true;
        $obtained = $account->equals($this->expected);
        $this->assertSame($expected, $obtained);

        return $account;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     * @test
     * @group           generated
     * @covers          ::fromArray
     * @depends         setCreatedAt
     * @depends         setEmailAddress
     * @depends         setEmailVerified
     * @depends         setIdentifier
     * @depends         setPassword
     * @depends         setUpdatedAt
     */
    public function fromArray(Model\Account $account): Model\Account
    {
        $typifier = Model\Map\AccountTableMap::TYPE_COLNAME;
        $indexers = Model\Map\AccountTableMap::getFieldNames($typifier);

        $expected = $this->expected->copy()->setIdentifier($this->expected->getIdentifier());
        ($obtained = new Model\Account())->fromArray([
            $indexers[0] => $this->expected->getEmailAddress(),
            $indexers[1] => $this->expected->getEmailVerified(),
            $indexers[2] => $this->expected->getPassword(),
            $indexers[3] => $this->expected->getCreatedAt(),
            $indexers[4] => $this->expected->getIdentifier(),
            $indexers[5] => $this->expected->getUpdatedAt()
        ], $typifier);
        $this->assertEquals($expected, $obtained);

        return $account;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     * @test
     * @group           generated
     * @covers          ::getByName
     * @depends         setByName
     */
    public function getByName(Model\Account $account): Model\Account
    {
        $expected = $this->expected->getCreatedAt();
        $obtained = $account->getByName(
            Model\Map\AccountTableMap::COL_CREATED_AT,
            Model\Map\AccountTableMap::TYPE_COLNAME
        );
        $this->assertEquals($expected, $obtained);

        $expected = $this->expected->getEmailAddress();
        $obtained = $account->getByName(
            Model\Map\AccountTableMap::COL_EMAIL_ADDRESS,
            Model\Map\AccountTableMap::TYPE_COLNAME
        );
        $this->assertSame($expected, $obtained);

        $expected = $this->expected->getEmailVerified();
        $obtained = $account->getByName(
            Model\Map\AccountTableMap::COL_EMAIL_VERIFIED,
            Model\Map\AccountTableMap::TYPE_COLNAME
        );
        $this->assertSame($expected, $obtained);

        $expected = $this->expected->getIdentifier();
        $obtained = $account->getByName(
            Model\Map\AccountTableMap::COL_IDENTIFIER,
            Model\Map\AccountTableMap::TYPE_COLNAME
        );
        $this->assertSame($expected, $obtained);

        $expected = $this->expected->getPassword();
        $obtained = $account->getByName(
            Model\Map\AccountTableMap::COL_PASSWORD,
            Model\Map\AccountTableMap::TYPE_COLNAME
        );
        $this->assertSame($expected, $obtained);

        $expected = $this->expected->getUpdatedAt();
        $obtained = $account->getByName(
            Model\Map\AccountTableMap::COL_UPDATED_AT,
            Model\Map\AccountTableMap::TYPE_COLNAME
        );
        $this->assertEquals($expected, $obtained);

        return $account;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     * @test
     * @group           generated
     * @covers          ::getByPosition
     * @depends         setByPosition
     */
    public function getByPosition(Model\Account $account): Model\Account
    {
        $expected = null;
        $obtained = $account->getByPosition(-1);
        $this->assertSame($expected, $obtained);

        $expected = $this->expected->getEmailAddress();
        $obtained = $account->getByPosition(0);
        $this->assertSame($expected, $obtained);

        $expected = $this->expected->getEmailVerified();
        $obtained = $account->getByPosition(1);
        $this->assertSame($expected, $obtained);

        $expected = $this->expected->getPassword();
        $obtained = $account->getByPosition(2);
        $this->assertSame($expected, $obtained);

        $expected = $this->expected->getCreatedAt();
        $obtained = $account->getByPosition(3);
        $this->assertEquals($expected, $obtained);

        $expected = $this->expected->getIdentifier();
        $obtained = $account->getByPosition(4);
        $this->assertSame($expected, $obtained);

        $expected = $this->expected->getUpdatedAt();
        $obtained = $account->getByPosition(5);
        $this->assertEquals($expected, $obtained);

        return $account;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     * @test
     * @group           generated
     * @covers          ::getCreatedAt
     * @depends         setCreatedAt
     */
    public function getCreatedAt(Model\Account $account): Model\Account
    {
        $expected = $this->expected->getCreatedAt();
        $obtained = $account->getCreatedAt();
        $this->assertEquals($expected, $obtained);

        $expected = $this->expected->getCreatedAt()->format('Y-m-d H:i:s.u');
        $obtained = $account->getCreatedAt('Y-m-d H:i:s.u');
        $this->assertSame($expected, $obtained);

        return $account;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     * @test
     * @group           generated
     * @covers          ::getEmailAddress
     * @depends         setEmailAddress
     */
    public function getEmailAddress(Model\Account $account): Model\Account
    {
        $expected = $this->expected->getEmailAddress();
        $obtained = $account->getEmailAddress();
        $this->assertSame($expected, $obtained);

        return $account;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     * @test
     * @group           generated
     * @covers          ::getEmailVerified
     * @depends         setEmailVerified
     */
    public function getEmailVerified(Model\Account $account): Model\Account
    {
        $expected = $this->expected->getEmailVerified();
        $obtained = $account->getEmailVerified();
        $this->assertSame($expected, $obtained);

        return $account;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     * @test
     * @group           generated
     * @covers          ::getIdentifier
     * @depends         setIdentifier
     */
    public function getIdentifier(Model\Account $account): Model\Account
    {
        $expected = $this->expected->getIdentifier();
        $obtained = $account->getIdentifier();
        $this->assertSame($expected, $obtained);

        return $account;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     * @test
     * @group           generated
     * @covers          ::getModifiedColumns
     * @depends         setCreatedAt
     * @depends         setEmailAddress
     * @depends         setEmailVerified
     * @depends         setIdentifier
     * @depends         setPassword
     * @depends         setUpdatedAt
     */
    public function getModifiedColumns(Model\Account $account): Model\Account
    {
        $expected = [
            Model\Map\AccountTableMap::COL_CREATED_AT,
            Model\Map\AccountTableMap::COL_EMAIL_ADDRESS,
            Model\Map\AccountTableMap::COL_EMAIL_VERIFIED,
            Model\Map\AccountTableMap::COL_IDENTIFIER,
            Model\Map\AccountTableMap::COL_PASSWORD,
            Model\Map\AccountTableMap::COL_UPDATED_AT
        ];
        $obtained = $account->getModifiedColumns();
        $this->assertSame($expected, $obtained);

        return $account;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     * @test
     * @group           generated
     * @covers          ::getPassword
     * @depends         setPassword
     */
    public function getPassword(Model\Account $account): Model\Account
    {
        $expected = $this->expected->getPassword();
        $obtained = $account->getPassword();
        $this->assertSame($expected, $obtained);

        return $account;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     * @test
     * @group           generated
     * @covers          ::getPrimaryKey
     * @depends         setPrimaryKey
     */
    public function getPrimaryKey(Model\Account $account): Model\Account
    {
        $expected = $this->expected->getIdentifier();
        $obtained = $account->getPrimaryKey();
        $this->assertSame($expected, $obtained);

        return $account;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     * @test
     * @group           generated
     * @covers          ::getUpdatedAt
     * @depends         setUpdatedAt
     */
    public function getUpdatedAt(Model\Account $account): Model\Account
    {
        $expected = $this->expected->getUpdatedAt();
        $obtained = $account->getUpdatedAt();
        $this->assertEquals($expected, $obtained);

        $expected = $this->expected->getUpdatedAt()->format('Y-m-d H:i:s.u');
        $obtained = $account->getUpdatedAt('Y-m-d H:i:s.u');
        $this->assertSame($expected, $obtained);

        return $account;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     * @test
     * @group           generated
     * @covers          ::getVirtualColumn
     * @depends         setVirtualColumn
     */
    public function getVirtualColumn(Model\Account $account): Model\Account
    {
        $expected = $this->expected->getVirtualColumn(Model\Map\AccountTableMap::TABLE_NAME);
        $obtained = $account->getVirtualColumn(Model\Map\AccountTableMap::TABLE_NAME);
        $this->assertSame($expected, $obtained);

        try {
            $account->getVirtualColumn(null);
        } catch (Throwable $throwable) {
            $expected = PropelException::class;
            $obtained = $throwable;
            $this->assertInstanceOf($expected, $obtained);
        }

        return $account;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     * @test
     * @group           generated
     * @covers          ::getVirtualColumns
     * @depends         setVirtualColumn
     */
    public function getVirtualColumns(Model\Account $account): Model\Account
    {
        $expected = [
            Model\Map\AccountTableMap::TABLE_NAME => $this->expected->getVirtualColumn(
                Model\Map\AccountTableMap::TABLE_NAME
            )
        ];
        $obtained = $account->getVirtualColumns();
        $this->assertSame($expected, $obtained);

        return $account;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     * @test
     * @group           generated
     * @covers          ::hasOnlyDefaultValues
     * @depends         applyDefaultValues
     */
    public function hasOnlyDefaultValues(Model\Account $account): Model\Account
    {
        $expected = false;
        $obtained = $account->copy()->setEmailVerified(true)->hasOnlyDefaultValues();
        $this->assertSame($expected, $obtained);

        $expected = true;
        $obtained = $account->hasOnlyDefaultValues();
        $this->assertSame($expected, $obtained);

        return $account;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     * @test
     * @group           generated
     * @covers          ::hasVirtualColumn
     * @depends         setVirtualColumn
     */
    public function hasVirtualColumn(Model\Account $account): Model\Account
    {
        $expected = true;
        $obtained = $account->hasVirtualColumn(Model\Map\AccountTableMap::TABLE_NAME);
        $this->assertSame($expected, $obtained);

        return $account;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     * @test
     * @group           generated
     * @covers          ::hashCode
     * @depends         setPrimaryKey
     */
    public function hashCode(Model\Account $account): Model\Account
    {
        $expected = crc32(json_encode($account->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        $obtained = $account->hashCode();
        $this->assertSame($expected, $obtained);

        $expected = spl_object_hash($copy = $account->copy());
        $obtained = $copy->hashCode();
        $this->assertSame($expected, $obtained);

        return $account;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     * @test
     * @group           generated
     * @covers          ::isColumnModified
     * @depends         setCreatedAt
     * @depends         setEmailAddress
     * @depends         setEmailVerified
     * @depends         setIdentifier
     * @depends         setPassword
     * @depends         setUpdatedAt
     */
    public function isColumnModified(Model\Account $account): Model\Account
    {
        $expected = true;
        $obtained = $account->isColumnModified(Model\Map\AccountTableMap::COL_CREATED_AT);
        $this->assertSame($expected, $obtained);

        $expected = true;
        $obtained = $account->isColumnModified(Model\Map\AccountTableMap::COL_EMAIL_ADDRESS);
        $this->assertSame($expected, $obtained);

        $expected = true;
        $obtained = $account->isColumnModified(Model\Map\AccountTableMap::COL_EMAIL_VERIFIED);
        $this->assertSame($expected, $obtained);

        $expected = true;
        $obtained = $account->isColumnModified(Model\Map\AccountTableMap::COL_IDENTIFIER);
        $this->assertSame($expected, $obtained);

        $expected = true;
        $obtained = $account->isColumnModified(Model\Map\AccountTableMap::COL_PASSWORD);
        $this->assertSame($expected, $obtained);

        $expected = true;
        $obtained = $account->isColumnModified(Model\Map\AccountTableMap::COL_UPDATED_AT);
        $this->assertSame($expected, $obtained);

        return $account;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     * @test
     * @group           generated
     * @covers          ::isDeleted
     * @depends         setDeleted
     */
    public function isDeleted(Model\Account $account): Model\Account
    {
        $expected = $this->expected->isDeleted();
        $obtained = $account->isDeleted();
        $this->assertSame($expected, $obtained);

        return $account;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     * @test
     * @group           generated
     * @covers          ::isEmailVerified
     * @depends         setEmailVerified
     */
    public function isEmailVerified(Model\Account $account): Model\Account
    {
        $expected = $this->expected->isEmailVerified();
        $obtained = $account->isEmailVerified();
        $this->assertSame($expected, $obtained);

        return $account;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     * @test
     * @group           generated
     * @covers          ::isModified
     * @depends         setCreatedAt
     * @depends         setEmailAddress
     * @depends         setEmailVerified
     * @depends         setIdentifier
     * @depends         setPassword
     * @depends         setUpdatedAt
     */
    public function isModified(Model\Account $account): Model\Account
    {
        $expected = true;
        $obtained = $account->isModified();
        $this->assertSame($expected, $obtained);

        return $account;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     * @test
     * @group           generated
     * @covers          ::isNew
     * @depends         setNew
     */
    public function isNew(Model\Account $account): Model\Account
    {
        $expected = $this->expected->isNew();
        $obtained = $account->isNew();
        $this->assertSame($expected, $obtained);

        return $account;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     * @test
     * @group           generated
     * @covers          ::isPrimaryKeyNull
     * @depends         setPrimaryKey
     */
    public function isPrimaryKeyNull(Model\Account $account): Model\Account
    {
        $expected = false;
        $obtained = $account->isPrimaryKeyNull();
        $this->assertSame($expected, $obtained);

        return $account;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     * @test
     * @group           generated
     * @covers          ::postDelete
     * @depends         delete
     */
    public function postDelete(Model\Account $account): Model\Account
    {
        $expected = null;
        $obtained = $account->postDelete();
        $this->assertSame($expected, $obtained);

        return $account;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     * @test
     * @group           generated
     * @covers          ::postInsert
     * @depends         save
     */
    public function postInsert(Model\Account $account): Model\Account
    {
        $expected = null;
        $obtained = $account->postInsert();
        $this->assertSame($expected, $obtained);

        return $account;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     * @test
     * @group           generated
     * @covers          ::postSave
     * @depends         save
     */
    public function postSave(Model\Account $account): Model\Account
    {
        $expected = null;
        $obtained = $account->postSave();
        $this->assertSame($expected, $obtained);

        return $account;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     * @test
     * @group           generated
     * @covers          ::postUpdate
     * @depends         save
     */
    public function postUpdate(Model\Account $account): Model\Account
    {
        $expected = null;
        $obtained = $account->postUpdate();
        $this->assertSame($expected, $obtained);

        return $account;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     * @test
     * @group           generated
     * @covers          ::preDelete
     * @depends         postInsert
     * @depends         postSave
     * @depends         postUpdate
     */
    public function preDelete(Model\Account $account): Model\Account
    {
        $expected = true;
        $obtained = $account->preDelete();
        $this->assertSame($expected, $obtained);

        return $account;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     * @test
     * @group           generated
     * @covers          ::preInsert
     * @depends         toArray
     */
    public function preInsert(Model\Account $account): Model\Account
    {
        $expected = true;
        $obtained = $account->preInsert();
        $this->assertSame($expected, $obtained);

        return $account;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     * @test
     * @group           generated
     * @covers          ::preSave
     * @depends         toArray
     */
    public function preSave(Model\Account $account): Model\Account
    {
        $expected = true;
        $obtained = $account->preSave();
        $this->assertSame($expected, $obtained);

        return $account;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     * @test
     * @group           generated
     * @covers          ::preUpdate
     * @depends         toArray
     */
    public function preUpdate(Model\Account $account): Model\Account
    {
        $expected = true;
        $obtained = $account->preUpdate();
        $this->assertSame($expected, $obtained);

        return $account;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     * @test
     * @group           generated
     * @covers          ::save
     * @depends         preInsert
     * @depends         preSave
     * @depends         preUpdate
     */
    public function save(Model\Account $account): Model\Account
    {
        $account = $account->copy();

        $expected = 2;
        $obtained = $account->save();
        $this->assertSame($expected, $obtained);

        $expected = false;
        $obtained = $account->isNew();
        $this->assertSame($expected, $obtained);

        $expected = 1;
        $obtained = $account->setEmailVerified($this->expected->getEmailVerified())->save();
        $this->assertSame($expected, $obtained);

        try {
            ($deleted = $account->copy())->setDeleted(true);
            $deleted->save();
        } catch (Throwable $throwable) {
            $expected = PropelException::class;
            $obtained = $throwable;
            $this->assertInstanceOf($expected, $obtained);
        }

        return $account;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     * @test
     * @group           generated
     * @covers          ::setByName
     * @depends         construct
     */
    public function setByName(Model\Account $account): Model\Account
    {
        $expected = Model\Account::class;
        $obtained = $account->setByName(
            Model\Map\AccountTableMap::COL_CREATED_AT,
            $this->expected->getCreatedAt(),
            Model\Map\AccountTableMap::TYPE_COLNAME
        );
        $this->assertInstanceOf($expected, $obtained);

        $expected = Model\Account::class;
        $obtained = $account->setByName(
            Model\Map\AccountTableMap::COL_EMAIL_ADDRESS,
            $this->expected->getEmailAddress(),
            Model\Map\AccountTableMap::TYPE_COLNAME
        );
        $this->assertInstanceOf($expected, $obtained);

        $expected = Model\Account::class;
        $obtained = $account->setByName(
            Model\Map\AccountTableMap::COL_EMAIL_VERIFIED,
            $this->expected->getEmailVerified(),
            Model\Map\AccountTableMap::TYPE_COLNAME
        );
        $this->assertInstanceOf($expected, $obtained);

        $expected = Model\Account::class;
        $obtained = $account->setByName(
            Model\Map\AccountTableMap::COL_IDENTIFIER,
            $this->expected->getIdentifier(),
            Model\Map\AccountTableMap::TYPE_COLNAME
        );
        $this->assertInstanceOf($expected, $obtained);

        $expected = Model\Account::class;
        $obtained = $account->setByName(
            Model\Map\AccountTableMap::COL_PASSWORD,
            $this->expected->getPassword(),
            Model\Map\AccountTableMap::TYPE_COLNAME
        );
        $this->assertInstanceOf($expected, $obtained);

        $expected = Model\Account::class;
        $obtained = $account->setByName(
            Model\Map\AccountTableMap::COL_UPDATED_AT,
            $this->expected->getUpdatedAt(),
            Model\Map\AccountTableMap::TYPE_COLNAME
        );
        $this->assertInstanceOf($expected, $obtained);

        return $account;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     * @test
     * @group           generated
     * @covers          ::setByPosition
     * @depends         construct
     */
    public function setByPosition(Model\Account $account): Model\Account
    {
        $expected = Model\Account::class;
        $obtained = $account->setByPosition(0, $this->expected->getEmailAddress());
        $this->assertInstanceOf($expected, $obtained);

        $expected = Model\Account::class;
        $obtained = $account->setByPosition(1, $this->expected->getEmailVerified());
        $this->assertInstanceOf($expected, $obtained);

        $expected = Model\Account::class;
        $obtained = $account->setByPosition(2, $this->expected->getPassword());
        $this->assertInstanceOf($expected, $obtained);

        $expected = Model\Account::class;
        $obtained = $account->setByPosition(3, $this->expected->getCreatedAt());
        $this->assertInstanceOf($expected, $obtained);

        $expected = Model\Account::class;
        $obtained = $account->setByPosition(4, $this->expected->getIdentifier());
        $this->assertInstanceOf($expected, $obtained);

        $expected = Model\Account::class;
        $obtained = $account->setByPosition(5, $this->expected->getUpdatedAt());
        $this->assertInstanceOf($expected, $obtained);

        return $account;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     * @test
     * @group           generated
     * @covers          ::setCreatedAt
     * @depends         construct
     */
    public function setCreatedAt(Model\Account $account): Model\Account
    {
        $expected = Model\Account::class;
        $obtained = $account->setCreatedAt($this->expected->getCreatedAt());
        $this->assertInstanceOf($expected, $obtained);

        return $account;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     * @test
     * @group           generated
     * @covers          ::setDeleted
     * @depends         construct
     */
    public function setDeleted(Model\Account $account): Model\Account
    {
        $expected = null;
        $obtained = $account->setDeleted($this->expected->isDeleted());
        $this->assertSame($expected, $obtained);

        return $account;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     * @test
     * @group           generated
     * @covers          ::setEmailAddress
     * @depends         construct
     */
    public function setEmailAddress(Model\Account $account): Model\Account
    {
        $expected = Model\Account::class;
        $obtained = $account->setEmailAddress($this->expected->getEmailAddress());
        $this->assertInstanceOf($expected, $obtained);

        return $account;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     * @test
     * @group           generated
     * @covers          ::setEmailVerified
     * @depends         construct
     */
    public function setEmailVerified(Model\Account $account): Model\Account
    {
        $expected = Model\Account::class;
        $obtained = $account->setEmailVerified((string) $this->expected->getEmailVerified());
        $this->assertInstanceOf($expected, $obtained);

        $expected = Model\Account::class;
        $obtained = $account->setEmailVerified($this->expected->getEmailVerified());
        $this->assertInstanceOf($expected, $obtained);

        return $account;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     * @test
     * @group           generated
     * @covers          ::setIdentifier
     * @depends         construct
     */
    public function setIdentifier(Model\Account $account): Model\Account
    {
        $expected = Model\Account::class;
        $obtained = $account->setIdentifier($this->expected->getIdentifier());
        $this->assertInstanceOf($expected, $obtained);

        return $account;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     * @test
     * @group           generated
     * @covers          ::setNew
     * @depends         construct
     */
    public function setNew(Model\Account $account): Model\Account
    {
        $expected = null;
        $obtained = $account->setNew($this->expected->isNew());
        $this->assertSame($expected, $obtained);

        return $account;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     * @test
     * @group           generated
     * @covers          ::setPassword
     * @depends         construct
     */
    public function setPassword(Model\Account $account): Model\Account
    {
        $expected = Model\Account::class;
        $obtained = $account->setPassword($this->expected->getPassword());
        $this->assertInstanceOf($expected, $obtained);

        return $account;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     * @test
     * @group           generated
     * @covers          ::setPrimaryKey
     * @depends         construct
     */
    public function setPrimaryKey(Model\Account $account): Model\Account
    {
        $expected = null;
        $obtained = $account->setPrimaryKey($this->expected->getIdentifier());
        $this->assertSame($expected, $obtained);

        return $account;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     * @test
     * @group           generated
     * @covers          ::setUpdatedAt
     * @depends         construct
     */
    public function setUpdatedAt(Model\Account $account): Model\Account
    {
        $expected = Model\Account::class;
        $obtained = $account->setUpdatedAt($this->expected->getUpdatedAt());
        $this->assertInstanceOf($expected, $obtained);

        return $account;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     * @test
     * @group           generated
     * @covers          ::setVirtualColumn
     * @depends         construct
     */
    public function setVirtualColumn(Model\Account $account): Model\Account
    {
        $expected = Model\Account::class;
        $obtained = $account->setVirtualColumn(
            Model\Map\AccountTableMap::TABLE_NAME,
            $this->expected->getVirtualColumn(Model\Map\AccountTableMap::TABLE_NAME)
        );
        $this->assertInstanceOf($expected, $obtained);

        return $account;
    }

    /**
     * @author          Gabriel Pereira de Barros <opereirabarros@outlook.com>
     * @copyright       © 2020 Gabriel Pereira de Barros. All Rights Reserved.
     * @since           1.0.0
     * @test
     * @group           generated
     * @covers          ::toArray
     * @depends         setCreatedAt
     * @depends         setEmailAddress
     * @depends         setEmailVerified
     * @depends         setIdentifier
     * @depends         setPassword
     * @depends         setUpdatedAt
     */
    public function toArray(Model\Account $account): Model\Account
    {
        $typifier = Model\Map\AccountTableMap::TYPE_COLNAME;
        $indexers = array_merge(
            Model\Map\AccountTableMap::getFieldNames($typifier),
            [
                Model\Map\AccountTableMap::TABLE_NAME
            ]
        );

        $expected = [
            $indexers[0] => $this->expected->getEmailAddress(),
            $indexers[1] => $this->expected->getEmailVerified(),
            $indexers[2] => $this->expected->getPassword(),
            $indexers[3] => $this->expected->getCreatedAt()->format('c'),
            $indexers[4] => $this->expected->getIdentifier(),
            $indexers[5] => $this->expected->getUpdatedAt()->format('c'),
            $indexers[6] => $this->expected->getVirtualColumn($indexers[6])
        ];
        $obtained = $account->toArray($typifier);
        $this->assertSame($expected, $obtained);

        $expected = '*RECURSION*';
        $obtained = $account->toArray($typifier, true, [
            (new ReflectionClass($account))->getShortName() => [$account->hashCode() => true]
        ]);
        $this->assertSame($expected, $obtained);

        return $account;
    }
}
