# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com), and this project adheres to
[Semantic Versioning](https://semver.org).

## [Unreleased]

## [1.7.1] - 2020-07-16

### Changed
- Increase data size in criteria pagination middleware.

## [1.7.0] - 2020-07-15

### Added
- Criteria pagination middleware.

### Changed
- Update of all dependencies managed by composer.

## [1.6.0] - 2020-07-13

### Added
- Criteria middleware.
- Criteria field middleware.
- Criteria session middleware.
- Criteria argument middleware.
- Criteria validation middleware.
- Vector model formatter utility.
- Translation fallback configuration.
- Allow column specification in select statements.
- Allow use of dots in route arguments definition.

### Changed
- Portuguese of Brazil as default language.
- Update of all dependencies managed by composer.
- Docker image based on Ebola 1.3.8.
- Docker image based on Dengue 1.1.21.
- Specification of Corona 1.5.7 as a Continuous Integration image.
- Specification of Docker 19.03.12 as a Continuous Integration image.
- Specification of Docker DinD 19.03.12 as a Continuous Integration service.
- Specification for generate `snake_case` fields when parsing model classes.

## [1.5.3] - 2020-06-28

### Fixed
- Unsupported `snake_case` statements.

## [1.5.2] - 2020-06-28

### Changed
- Specification for generate `camelCase` fields when parsing model classes.
- Update of all dependencies managed by composer.

## [1.5.1] - 2020-06-14

### Changed
- Docker image based on Dengue 1.1.19.
- Docker image based on Ebola 1.3.6.
- Specification of Corona 1.5.5 as a Continuous Integration image.

## [1.5.0] - 2020-06-14

### Added
- Route configuration for a patient document update handler resolver.
- Patient document update handler resolver.
- Route configuration for a patient document delete handler resolver.
- Patient document delete handler resolver.
- Route configuration for a patient document read handler resolver.
- Patient document read handler resolver.
- Route configuration for a patient document create handler resolver.
- Patient document create handler resolver.
- Person entity schema.
- Patient document entity schema.
- Media entity schema.

### Changed
- Update of all dependencies managed by composer.

## [1.4.0] - 2020-06-11

### Added
- Geolocation entity schema.

## [1.3.0] - 2020-06-10

### Added
- Route configuration for a patient address update handler resolver.
- Patient address update handler resolver.
- Route configuration for a patient address delete handler resolver.
- Patient address delete handler resolver.
- Route configuration for a patient address read handler resolver.
- Patient address read handler resolver.
- Route configuration for a patient address create handler resolver.
- Patient address create handler resolver.
- Patient address entity schema.
- Address entity schema.
- Record entity schema.

## [1.2.0] - 2020-06-07

### Added
- Route configuration for a patient update handler resolver.
- Patient update handler resolver.
- Route configuration for a patient delete handler resolver.
- Patient delete handler resolver.
- Route configuration for a patient read handler resolver.
- Patient read handler resolver.
- Route configuration for a patient create handler resolver.
- Patient create handler resolver.
- Patient entity schema.

## [1.1.8] - 2020-06-03

### Changed
- Specification of Corona 1.5.4 as a Continuous Integration image.
- Specification of Docker 19.03.11 as a Continuous Integration image.
- Specification of Docker DinD 19.03.11 as a Continuous Integration service.
- Docker image based on Ebola 1.3.5.
- Docker image based on Dengue 1.1.18.
- Update of all dependencies managed by composer.

## [1.1.7] - 2020-05-27

### Changed
- Specification of Corona 1.5.3 as a Continuous Integration image.
- Specification of MariaDB 10.5.3 as a Continuous Integration service.
- Specification of Docker 19.03.9 as a Continuous Integration image.
- Specification of Docker DinD 19.03.9 as a Continuous Integration service.
- Docker image based on Ebola 1.3.4.
- Docker image based on Dengue 1.1.17.
- Update of all dependencies managed by composer.

### Fixed
- Unnecessary period in email subject.

## [1.1.6] - 2020-05-04

### Fixed
- Include model configuration files in Ebola build context.

## [1.1.5] - 2020-05-04

### Changed
- Enable Docker Build Kit in Continuous Integration release stage.

## [1.1.4] - 2020-05-04

### Fixed
- Isolation of preparation scripts in Continuous Integration.

## [1.1.3] - 2020-05-04

### Changed
- Docker image based on Ebola 1.3.3.
- Docker image based on Dengue 1.1.16.
- Update of all dependencies managed by composer.

## [1.1.2] - 2020-04-27

### Changed
- Specification of MariaDB 10.5.2 as a Continuous Integration service.
- Docker image based on Dengue 1.1.15.
- Update of all dependencies managed by composer.

### Removed
- Theme attribute from display entity schema.

## [1.1.1] - 2020-04-20

### Changed
- Update of all dependencies managed by composer.

## [1.1.0] - 2020-04-20

### Added
- Update date attribute in access entity schema.
- Creation date attribute in display entity schema.

## [1.0.5] - 2020-04-20

### Changed
- Clearer indentation in factory utilities.
- Explicit language reference in translation utilities.
- Explicit language reference in display entity schema.
- Specialization of display preference resources.

### Fixed
- Unnecessary error throw in exceptions middleware.

## [1.0.4] - 2020-04-19

### Changed
- Cardinality of the relationship between an account and a preference.

### Fixed
- Possibility of a null identifier when removing an access.
- `JWT_SECRET_KEY_SESSION` environment variable name in usage demonstration.
- `JWT_SECRET_KEY_VALIDATION` environment variable name in usage demonstration.

### Removed
- Unnecessary verification of relationship between an account and a preference.

## [1.0.3] - 2020-04-18

### Changed
- Usage demonstration with recommended parameters.

## [1.0.2] - 2020-04-18

### Changed
- `SECRET_KEY_SESSION` environment variable renamed to `JWT_SECRET_KEY_SESSION`.
- `SECRET_KEY_VALIDATION` environment variable renamed to `JWT_SECRET_KEY_VALIDATION`.

## [1.0.1] - 2020-04-17

### Changed
- Display errors on the console.

## [1.0.0] - 2020-04-17

### Added
- Project readme.
- Translation agent factory utility.
- Translation agent configuration utility.
- Translation agent configuration proxy utility.
- Translation resource utility for Portuguese of Brazil.
- Translation resource utility for English of United States.
- Covenant for a translation resource utility.
- [Symfony Translation](https://packagist.org/packages/symfony/translation) as dependency.
- Route configuration for a preference update handler resolver.
- Preference update handler resolver.
- Route configuration for a preference read handler resolver.
- Preference read handler resolver.
- Preference entity schema.
- Email sender factory utility.
- Email message factory utility.
- Email message configuration utility.
- Email transport factory utility.
- Email transport configuration utility.
- Validation middleware.
- Route configuration for a validation create handler resolver.
- Validation create handler resolver.
- Validation entity schema.
- IP address detector middleware.
- Browser detector middleware.
- System detector middleware.
- [Device Detector](https://packagist.org/packages/piwik/device-detector) as dependency.
- Route configuration for an access delete handler resolver.
- Access delete handler resolver.
- Route configuration for an access read handler resolver.
- Access read handler resolver.
- Access entity schema.
- [Symfony Mailer](https://packagist.org/packages/symfony/mailer) as dependency.
- Route configuration for a session delete handler resolver.
- Session delete handler resolver.
- Route configuration for a session read handler resolver.
- Session read handler resolver.
- CORS middleware.
- JWT model encoder utility.
- JSON model encoder utility.
- Covenant for a model encoder utility.
- Record collection model parser utility.
- Record model parser utility.
- Covenant for a model parser utility.
- Route configuration for a session update handler resolver.
- Session update handler resolver.
- Route configuration for a session create handler resolver.
- Session create handler resolver.
- JWT exception.
- Session middleware.
- Session entity schema.
- [PHP-JWT](https://packagist.org/packages/firebase/php-jwt) as dependency.
- Content type middleware.
- Content length middleware.
- Default entrypoint.
- App factory utility.
- App configuration utility.
- App configuration utility proxy.
- Route configuration for an account update handler resolver.
- Account update handler resolver.
- Route configuration for an account delete handler resolver.
- Account delete handler resolver.
- Route configuration for an account read handler resolver.
- Account read handler resolver.
- Route configuration for an account create handler resolver.
- Account create handler resolver.
- JWT exception handler resolver.
- PDO exception handler resolver.
- HTTP exception handler resolver.
- Covenant for a exception handler resolver.
- Exception middleware.
- Covenant for a handler resolver.
- Covenant for a route configuration.
- Documentation.
- PSR-12 source code checker in unit test.
- Unit test.
- Unified bootstrap file.
- Docker image based on Ebola 1.3.2.
- Docker image based on Dengue 1.1.14.
- Specification of Corona 1.5.2 as a Continuous Integration image.
- Specification of MariaDB 10.4.12 as a Continuous Integration service.
- Specification of Docker 19.03.8 as a Continuous Integration image.
- Specification of Docker DinD 19.03.8 as a Continuous Integration service.
- Continuous Integration.
- Contributing guide.
- Project license.
- Account entity schema.
- Database schema.
- Command to perform migrations on the database.
- Specification for generate `snake_case` fields when parsing model classes.
- Specification to build the model layer before refresh the autoload file.
- Command to build the model layer.
- Namespace for generated model layer.
- Specification of non-versioning the generated model layer.
- Configuration file for generating the model layer.
- [Propel](https://packagist.org/packages/propel/propel) as dependency.
- Configuration parameter to use unstable dependencies.
- Namespace configuration for project classes.
- [Slim PSR-7](https://packagist.org/packages/slim/psr7) as dependency.
- [Slim](https://packagist.org/packages/slim/slim) as dependency.
- Specification of non-versioning the dependencies directory.
- Manager for the project and its dependencies.
- Record of all notable changes made to this project.
