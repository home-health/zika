# Zika

Zika is an interface for information exchange and storage distributed through Docker images based on
[Ebola](https://gitlab.com/home-health/ebola) and [Dengue](https://gitlab.com/home-health/dengue).

## Authentication

Authenticate to the GitLab Container Registry before use these images.

```bash
docker login registry.gitlab.com -u <username> -p <token>
```

## Usage

Create a network to connect the containers.

```bash
docker network create zika
```

Create a database container.

```bash
docker run \
  --detach \
  --name database \
  --network zika \
  --env MYSQL_DATABASE='DIryCJCagUBCUnjRrvN2zL1uioOEPx1qIOkDP9LOdavR4GnJ7R9RKql3tYwF2pUW' \
  --env MYSQL_PASSWORD='Yq86VV7Cno2qYKfWq3Utm1Qwunhh4SzFsD1oSUStwOxesD9nIHgFyKO21BgzOkWO' \
  --env MYSQL_RANDOM_ROOT_PASSWORD='yes' \
  --env MYSQL_USER='zHfHnrTJeZLSGOs5EnXjtYfpGKPnWyyQZH6OlANwgpcqjR1F3RKym1GiaDIz3LIc' \
  mariadb
```

Create a process manager container.

```bash
docker run \
  --detach \
  --name ebola \
  --network zika \
  --env DATABASE_HOST='database' \
  --env DATABASE_NAME='DIryCJCagUBCUnjRrvN2zL1uioOEPx1qIOkDP9LOdavR4GnJ7R9RKql3tYwF2pUW' \
  --env DATABASE_PASSWORD='Yq86VV7Cno2qYKfWq3Utm1Qwunhh4SzFsD1oSUStwOxesD9nIHgFyKO21BgzOkWO' \
  --env DATABASE_USER='zHfHnrTJeZLSGOs5EnXjtYfpGKPnWyyQZH6OlANwgpcqjR1F3RKym1GiaDIz3LIc' \
  --env JWT_SECRET_KEY_SESSION='W7vGZ6SF66ek6LKTV2x3r0f1UADVwuxiL4XUyL2wmRhF6BkXmYne36mWRBnrPM3F' \
  --env JWT_SECRET_KEY_VALIDATION='K9Thk7D0e33hfibEbYEgFiWNxY1Y5lKImWNG7RIJkg2Sz6akVscXFXbK4mkcK7EG' \
  --env SMTP_ACCOUNT_EMAIL_ADDRESS='notification@home-health.com' \
  --env SMTP_ACCOUNT_NAME='Home Health' \
  --env SMTP_ACCOUNT_PASSWORD='xGxRl9nRDZnx3JePiqqLHT2y8qBayK0LjpJlhau7L9SzhGrSS1kNfDiSR7G18G6c' \
  --env SMTP_HOST_ADDRESS='smtp.home-health.com' \
  --env SMTP_HOST_PORT='587' \
  registry.gitlab.com/home-health/zika:ebola
```

Create a web server container.

```bash
docker run \
  --detach \
  --name dengue \
  --network zika \
  --publish 80:80 \
  registry.gitlab.com/home-health/zika:dengue
```

When running, the interface listens for requests on `127.0.0.1:80`.

## Contributing

See the [Contribution Guide](CONTRIBUTING.md).

## License

See the [End-User License Agreement (EULA)](LICENSE.md).
