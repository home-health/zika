<?php

declare(strict_types=1);

require __DIR__ . '/../configuration/bootstrap.php';

use HomeHealth\Zika\Utility\App;

(new App\Factory())->create()->run();
