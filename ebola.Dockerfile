FROM \
    registry.gitlab.com/home-health/ebola:1.3.8
COPY \
    . \
    .
CMD \
    composer run build \
    && \
    composer run migration \
    && \
    start
